import React, { useEffect } from 'react';
import Head from './components/common/Head';
import Layout from './components/common/Layout';
import { Router } from './routes/Router';
import ScrollToTop from './components/common/ScrollToTop/ScrollToTop';
import { useLocation } from 'react-router-dom';
import LoginModal from './components/ui/LoginModal';
import User from './lib/User';
import { useAppDispatch } from './store/store';
import { setUser } from 'store/slices/user';

function App() {
  const location = useLocation();
  const dispatch = useAppDispatch();

  const needHeaderFooter =
    !location.pathname.includes('/login') ||
    location.pathname.startsWith('/login/confirm');

  useEffect(() => {
    if (User.isUser()) {
      dispatch(setUser(User.getUser()));
    }
  }, []);

  return (
    <>
      <Head />
      <Layout needHeaderFooter={needHeaderFooter}>
        <ScrollToTop />
        <Router />
      </Layout>
      <LoginModal />
    </>
  );
}

export default App;
