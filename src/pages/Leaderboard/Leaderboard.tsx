import React, { useEffect } from 'react';
import s from './Leaderboard.module.scss';
import LeaderboardMain from 'components/LeaderboardPage/LeaderboardMain';
import { useAppDispatch } from 'store/store';
import { resetLeaderboard } from 'store/slices/leaderboard';

const Leaderboard = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    return () => {
      dispatch(resetLeaderboard());
    };
  }, [dispatch]);

  return (
    <div className={s.root}>
      <div className={s.container}>
        <LeaderboardMain />
      </div>
    </div>
  );
};

export default Leaderboard;
