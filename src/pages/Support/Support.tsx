import React from 'react';
import s from './Support.module.scss';
import Breadcrumbs from 'components/ui/Breadcrumbs';
import { Typography } from '@material-ui/core';
import SupportMain from 'components/SupportPage/SupportMain';

const Support = () => {
  return (
    <div className={s.root}>
      <div className={s.container}>
        <Breadcrumbs
          className={s.breadcrumbs}
          items={[
            { id: 1, href: `/help`, label: 'Help' },
            { id: 0, href: '/support', label: 'Contact us' },
          ]}
        />
        <Typography variant="h1" className={s.title}>
          Support
        </Typography>
        <SupportMain />
      </div>
    </div>
  );
};

export default Support;
