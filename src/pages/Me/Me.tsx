import React, { useState } from 'react';
import s from './Me.module.scss';
import { Typography } from '@material-ui/core';
import Breadcrumbs from '../../components/ui/Breadcrumbs';
import { RemoveIcon } from '../../components/icons';
import MePageWrapper from '../../components/MePage/MePageWrapper';
import { useAppDispatch, useSelector } from '../../store/store';
import { finishRegistrationSelector } from '../../store/selectors';
import MainApiProtected from '../../api/MainApiProtected';
import { createErrorToast, createSuccessToast } from '../../lib/utils/toasts';
import User from '../../lib/User';
import { logoutUser } from 'store/slices/user';
import { useHistory } from 'react-router-dom';

const Me = () => {
  const finishRegistrationData = useSelector(finishRegistrationSelector);
  const [submit, setSubmit] = useState(false);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleClick = async () => {
    // eslint-disable-next-line no-restricted-globals
    const answer = confirm('Are you sure?');

    if (answer) {
      setSubmit(true);
      const result = await MainApiProtected.getInstance().deleteUsersMe();

      if (result.status !== 200) {
        createErrorToast({
          message: 'Something went wrong, please try again',
        });
        setSubmit(false);

        return;
      }

      createSuccessToast({ message: 'Your account was deleted' });
      User.logout();
      dispatch(logoutUser());
      setSubmit(false);
      history.push('/');
    }
  };

  return (
    <div className={s.root}>
      <div className={s.container}>
        <Breadcrumbs
          className={s.breadcrumbs}
          items={[
            { id: 1, href: `/`, label: 'Home' },
            { id: 0, href: '/me', label: 'My account' },
          ]}
        />
        <Typography variant="h1" className={s.title}>
          Owner account
          {!finishRegistrationData.active && (
            <button
              type="button"
              className={s.deleteAccountP} // p => PC
              onClick={handleClick}
              disabled={submit}
            >
              <RemoveIcon />
              Delete account
            </button>
          )}
        </Typography>
        <MePageWrapper />
        {!finishRegistrationData.active && (
          <button
            type="button"
            className={s.deleteAccountM} // m => MOBILE
            onClick={handleClick}
            disabled={submit}
          >
            <RemoveIcon />
            Delete account
          </button>
        )}
      </div>
    </div>
  );
};

export default Me;
