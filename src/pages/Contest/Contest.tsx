import React, { useEffect } from 'react';
import s from './Contest.module.scss';
import ContestMain from 'components/ContestPage/ContestMain';
import { useAppDispatch } from 'store/store';
import { resetContest } from 'store/slices/contest';

const Contest = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    return () => {
      dispatch(resetContest());
    };
  }, [dispatch]);

  return (
    <div className={s.root}>
      <div className={s.container}>
        <ContestMain />
      </div>
    </div>
  );
};

export default Contest;
