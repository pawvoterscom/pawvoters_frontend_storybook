import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { CircularProgress } from '@material-ui/core';
import s from './LoginConfirmByHash.module.scss';
import MainApi from '../../api/MainApi';
import { createErrorToast, createSuccessToast } from '../../lib/utils/toasts';
import { UserAuthEnum } from '../../types/enum/UserAuth.enum';
import { useAppDispatch } from '../../store/store';
import { setUser } from '../../store/slices/user';
import User from '../../lib/User';
import { setRegisterFinishInfo } from '../../store/slices/app';

const LoginConfirmByHash = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const { hash } = useParams<{ hash: string }>();

  useEffect(() => {
    (async () => {
      try {
        const confirmResult = await MainApi.getInstance().postLoginConfirm(
          hash
        );

        console.log('confirm result: ', confirmResult);

        if (confirmResult.status === 422 || confirmResult.status === 404) {
          history.push('/');
          createErrorToast({ message: 'Check your link and try again' });

          return;
        }

        if (confirmResult.status === 403) {
          history.push('/');
          createErrorToast({ message: confirmResult.data.message });

          return;
        }

        const loginResult = await MainApi.getInstance().postLogin(
          UserAuthEnum.CUSTOM,
          confirmResult.data.token
        );

        console.log('login result: ', loginResult);

        if (loginResult.status !== 200) {
          history.push('/');
          createErrorToast({
            message: 'Something went wrong, please try again later',
          });

          return;
        }

        history.push('/');
        dispatch(setUser(loginResult.data));
        User.setUser(loginResult.data);
        createSuccessToast({ message: 'You have been logged in successfully' });
      } catch (error) {
        if (error.status === 404) {
          history.push('/me');
          createSuccessToast({ message: 'Please finish the registration' });

          dispatch(
            setRegisterFinishInfo({
              userEmail: error.data.userEmail,
              userToken: error.data.userToken,
              active: true,
            })
          );
          return;
        }
      }
    })();
  }, [dispatch, hash, history]);

  return (
    <div className={s.root}>
      <CircularProgress size={48} />
    </div>
  );
};

export default LoginConfirmByHash;
