import React, { useEffect } from 'react';
import cn from 'classnames';
import s from './Pet.module.scss';
import { useHistory, useParams } from 'react-router-dom';
import Breadcrumbs from 'components/ui/Breadcrumbs';
import { parsePetSlug } from 'lib/utils/url-utils';
import PetComments from 'components/PetPage/page/PetComments';
import SimilarPets from 'components/PetPage/page/SimilarPets';
import SpringModal from 'components/ui/SpringModal';
import { useAppDispatch, useSelector } from 'store/store';
import { petModalData, petReportReasonsSelector } from 'store/selectors';
import { handlePetModalState, resetPetState } from 'store/slices/pet';
import VoteForPetModal from 'components/PetPage/paymentModal/VoteForPetModal';
import { createErrorToast } from 'lib/utils/toasts';
import {
  fetchPostComments,
  fetchPostData,
  fetchPostVotes,
  fetchSimilarPosts,
  getReportReasons,
} from 'store/slices/pet/thunks';
import PetImage from 'components/PetPage/page/PetImage';
import PetBio from 'components/PetPage/page/PetBio';
import PetCredentials from 'components/PetPage/page/PetCredentials';
import PetOwner from 'components/PetPage/page/PetOwner';
import PetShare from 'components/PetPage/page/PetShare';
import PetVotesBlock from 'components/PetPage/page/PetVotesBlock';
import { LoadingStatusEnum } from '../../types/enum/LoadingStatus.enum';
import { useUser } from '../../lib/hooks/useUser';

const Pet = () => {
  const { slug } = useParams<{ slug: string }>();
  const { isAuth } = useUser();
  const [petName, petId] = parsePetSlug(slug);

  const modalState = useSelector(petModalData);
  const reportReasonsData = useSelector(petReportReasonsSelector);

  const history = useHistory();
  const dispatch = useAppDispatch();

  const handlePetModalClose = () => {
    dispatch(handlePetModalState({ opened: false }));
  };

  useEffect(() => {
    (async () => {
      const postId = parseInt(petId);
      const post = await dispatch(fetchPostData({ postId }));

      const data = post.payload?.data;

      if (data) {
        if (data.petName !== petName) {
          history.push('/');
          createErrorToast({ message: 'Post not found' });
        }
      }

      if (post.payload?.status !== 200) {
        history.push('/');
        return createErrorToast({
          message: 'Something went wrong, please try again',
        });
      }

      dispatch(fetchPostVotes({ postId }));
      dispatch(fetchPostComments({ postId }));
      dispatch(fetchSimilarPosts({ postId }));
    })();

    return () => {
      dispatch(resetPetState());
    };
  }, [dispatch, petId]);

  useEffect(() => {
    if (isAuth && reportReasonsData.status === LoadingStatusEnum.IDLE) {
      dispatch(getReportReasons({}));
    }
  }, [isAuth]);

  return (
    <div className={cn(s.root)}>
      <div className={s.container}>
        <Breadcrumbs
          className={s.breadcrumbs}
          items={[
            { id: 0, href: '/contest', label: 'Contest' },
            { id: 1, href: `/post/${slug}`, label: petName },
          ]}
        />
        <div className={cn(s.petInfoRoot)}>
          <PetImage />
          <div className={cn(s.petInfoCol)}>
            <PetCredentials />
            <PetOwner />
            <PetVotesBlock />
            <PetShare />
          </div>
        </div>
        <PetBio />
        <PetComments />
        <SimilarPets />
      </div>
      <SpringModal opened={modalState.opened} handleClose={handlePetModalClose}>
        <VoteForPetModal />
      </SpringModal>
    </div>
  );
};

export default Pet;
