import React from 'react';
import s from './Search.module.scss';
import SearchInput from 'components/SearchPage/SearchInput';
import SearchPosts from 'components/SearchPage/SearchListMain';

const Search = () => {
  return (
    <div className={s.root}>
      <div className={s.container}>
        <SearchInput />
        <SearchPosts />
      </div>
    </div>
  );
};

export default Search;
