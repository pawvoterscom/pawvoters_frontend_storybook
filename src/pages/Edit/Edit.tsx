import React, { useEffect, useState } from 'react';
import s from './Edit.module.scss';
import Breadcrumbs from 'components/ui/Breadcrumbs';
import { Typography } from '@material-ui/core';
import EditPageMain from 'components/EditPage/EditPageMain';
import { RemoveIcon } from 'components/icons';
import { useAppDispatch, useSelector } from 'store/store';
import { finishRegistrationSelector, petSelector } from 'store/selectors';
import MainApiProtected from '../../api/MainApiProtected';
import { createErrorToast, createSuccessToast } from '../../lib/utils/toasts';
import { useHistory, useParams } from 'react-router-dom';
import { petDataSelector } from 'store/selectors/index';
import { parsePetSlug } from 'lib/utils/url-utils';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { resetPetState } from 'store/slices/pet';

const Edit = () => {
  const [submit, setSubmit] = useState<boolean>(false);

  const { slug } = useParams<{ slug: string }>();
  const [petName, petId] = parsePetSlug(slug);

  const finishRegistrationData = useSelector(finishRegistrationSelector);
  const myPetData = useSelector(petDataSelector);
  const myPet = useSelector(petSelector);

  const history = useHistory();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (myPet.petData.status === LoadingStatusEnum.SUCCESS && myPetData) {
      if (myPetData.petName.toLowerCase() !== petName.toLowerCase()) {
        history.push('/pets');
        createErrorToast({ message: 'Post not found' });
      }
    }
  }, [myPetData]);

  useEffect(() => {
    return () => {
      dispatch(resetPetState());
    };
  }, [dispatch]);

  const handleClick = async () => {
    // eslint-disable-next-line no-restricted-globals
    const answer = confirm('Are you sure?');

    if (answer) {
      setSubmit(true);
      const result = await MainApiProtected.getInstance().deleteMyPet(petId);

      if (result.status !== 200) {
        createErrorToast({
          message: 'Something went wrong, please try again',
        });
        setSubmit(false);

        return;
      }

      createSuccessToast({ message: 'Your pet was deleted' });
      setSubmit(false);
      history.push('/pets');
    }
  };

  return (
    <div className={s.root}>
      <div className={s.container}>
        <Breadcrumbs
          className={s.breadcrumbs}
          items={[
            { id: 1, href: '/', label: 'Home' },
            { id: 2, href: '/pets', label: 'Edite Pet' },
            { id: 3, href: `/edit/${slug}`, label: `${petName}` },
          ]}
        />
        <Typography variant="h1" className={s.title}>
          Edite pet
          {!finishRegistrationData.active && (
            <button
              type="button"
              className={s.deleteAccountP} // p => PC
              onClick={handleClick}
              disabled={submit || !myPetData}
            >
              <RemoveIcon />
              Delete pet
            </button>
          )}
        </Typography>
        <EditPageMain />
        {!finishRegistrationData.active && (
          <button
            type="button"
            className={s.deleteAccountM} // m => MOBILE
            onClick={handleClick}
            disabled={submit || !myPetData}
          >
            <RemoveIcon />
            Delete pet
          </button>
        )}
      </div>
    </div>
  );
};

export default Edit;
