import React from 'react';
import '../styles/main.scss';
import { Story, Meta } from '@storybook/react/types-6-0';
import image from '../static/images/development/winnders_card.jpg';
import avatar from '../static/images/development/winners_card_avatar.jpg';

import WinnerCard from '../components/cards/WinnerCard';
import { WinnerCardProps } from '../components/cards/WinnerCard';

export default {
  title: 'Cards/Winner Card',
  component: WinnerCard,
} as Meta;

const Template: Story<WinnerCardProps> = (args) => (
  <div style={{ maxWidth: 366 }}>
    <WinnerCard {...args} />
  </div>
);

export const WinnerCardMain = Template.bind({});

WinnerCardMain.args = {
  items: [
    {
      id: 0,
      owner: {
        avatar: avatar,
        comment:
          'A fun competition where we can show off our lovely pets with a lovely bonus if we win - THANK YOU!',
        ownerName: 'Hazel L.',
      },
      pet: {
        image: image,
        petName: 'Luna1',
        wonAmount: 200,
      },
    },
    {
      id: 1,
      owner: {
        avatar: avatar,
        comment:
          'A fun competition where we can show off our lovely pets with a lovely bonus if we win - THANK YOU!',
        ownerName: 'Hazel L.',
      },
      pet: {
        image: image,
        petName: 'Luna2',
        wonAmount: 200,
      },
    },
    {
      id: 1,
      owner: {
        avatar: avatar,
        comment:
          'A fun competition where we can show off our lovely pets with a lovely bonus if we win - THANK YOU!',
        ownerName: 'Hazel L.',
      },
      pet: {
        image: image,
        petName: 'Luna3',
        wonAmount: 200,
      },
    },
  ],
};
