import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Button, { ButtonProps } from '../components/ui/Button';

export default {
  title: 'Buttons/Default Button',
  component: Button,
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  children: 'Primary button',
  disabled: false,
  variant: 'contained',
};

export const Secondary = Template.bind({});

Secondary.args = {
  children: 'Secondary button',
  disabled: false,
  variant: 'outlined',
};
