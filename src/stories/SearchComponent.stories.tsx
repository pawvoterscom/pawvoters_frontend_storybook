import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Search, SearchProps } from '../components/common/Search';

export default {
  title: 'Components/Search',
  component: Search,
} as Meta;

const Template: Story<SearchProps> = (args) => (
  <div style={{ maxWidth: 195 }}>
    <Search {...args} />
  </div>
);

export const SearchComponent = Template.bind({});

SearchComponent.args = {
  placeholder: 'Search by pet name',
};
