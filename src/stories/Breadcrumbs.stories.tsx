import React from 'react';
import '../styles/main.scss';
import { Story, Meta } from '@storybook/react/types-6-0';

import Breadcrumbs, { BreadcrumbsProps } from '../components/ui/Breadcrumbs';

export default {
  title: 'Components/Breadcrumbs',
  component: Breadcrumbs,
} as Meta;

const Template: Story<BreadcrumbsProps> = (args) => <Breadcrumbs {...args} />;

export const BreadcrumbsBase = Template.bind({});

BreadcrumbsBase.args = {
  items: [
    {
      id: 0,
      href: 'https://google.com',
      label: 'Active',
    },
    {
      id: 1,
      href: 'https://google.com',
      label: 'Hover',
    },
    {
      id: 2,
      href: 'https://google.com',
      label: 'Current',
    },
  ],
};
