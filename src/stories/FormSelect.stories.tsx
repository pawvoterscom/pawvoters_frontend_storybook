import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Select, SelectProps } from '../components/ui/Select';

export default {
  title: 'Form/Select',
  component: Select,
  args: {
    name: 'base_input',
    id: 'base_input',
    multiline: false,
  },
} as Meta;

const Template: Story<SelectProps> = (args) => (
  <div style={{ width: 231 }}>
    <Select {...args} />
  </div>
);

export const BaseSelect = Template.bind({});

BaseSelect.args = {
  disabled: false,
  label: 'Title',
  items: [
    {
      id: 0,
      value: 'Jack 1',
      placeholder: 'Value 1',
    },
    {
      id: 1,
      value: 'Jack 2',
      placeholder: 'Value 1',
    },
    {
      id: 2,
      value: 'Jack 3',
      placeholder: 'Value 1',
    },
  ],
};

export const DisabledSelect = Template.bind({});

DisabledSelect.args = {
  disabled: true,
  label: 'Title',
  items: [
    {
      id: 0,
      value: 'Jack 1',
      placeholder: 'Value 1',
    },
    {
      id: 1,
      value: 'Jack 2',
      placeholder: 'Value 1',
    },
    {
      id: 2,
      value: 'Jack 3',
      placeholder: 'Value 1',
    },
  ],
};
