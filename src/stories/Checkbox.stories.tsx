import React from 'react';
import '../styles/main.scss';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Checkbox, CheckboxProps } from '../components/ui/Checkbox';

export default {
  title: 'Form/Checkbox',
  component: Checkbox,
} as Meta;

const Template: Story<CheckboxProps> = (args) => <Checkbox {...args} />;

export const BaseCheckbox = Template.bind({});

BaseCheckbox.args = {
  disabled: false,
  label: 'Item',
};

export const DisabledCheckbox = Template.bind({});

DisabledCheckbox.args = {
  disabled: true,
  label: 'Item',
};
