import React, { useEffect, useState } from 'react';
import Switcher from 'components/ui/ContestSwitcher';
import { useHistory, useLocation } from 'react-router-dom';
import s from './ContestSwitcher.module.scss';
import { elements } from './elements';
import { useSelector } from 'react-redux';
import { contestSelector } from 'store/selectors';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { useAppDispatch } from 'store/store';
import { getContest, getContestWinners } from 'store/slices/contest/thunks';
import { LeaderBoardType } from 'types/enum/LeaderBoardType.enum';

interface FilterType {
  _type: string;
}

const ContestSwitcher = () => {
  const [active, setActive] = useState<string>('dogs');

  const location = useLocation();
  const dispatch = useAppDispatch();
  const history = useHistory();

  const data = useSelector(contestSelector);

  const contestChecker: string[] = location.search.split('&');
  const [type]: string[] = contestChecker;

  const filter: FilterType = {
    _type: type.slice(6),
  };

  const { _type } = filter;

  const validType: boolean =
    _type === LeaderBoardType.CATS ||
    _type === LeaderBoardType.DOGS ||
    _type === LeaderBoardType.WINNERS;

  useEffect(() => {
    const result = location.search.match(RegExp('winners|dogs|cats'))?.shift();

    setActive(result ? result : 'dogs');

    if (!validType) history.push('/contest');

    async function requests() {
      try {
        if (result === LeaderBoardType.WINNERS) {
          return dispatch(getContestWinners({}));
        }

        dispatch(
          getContest(result ? { type: result.slice(0, -1) } : { type: 'dog' })
        );
      } catch (error) {
        console.log(error);
      }
    }

    requests();
  }, [location.search]);

  const handleClick = (_type: string) => {
    if (data.status === LoadingStatusEnum.SUCCESS) {
      history.push(`/contest?type=${_type}`);
    }
  };

  return (
    <div className={s.switcher}>
      <Switcher active={active} onChange={handleClick} items={elements} />
    </div>
  );
};

export default ContestSwitcher;
