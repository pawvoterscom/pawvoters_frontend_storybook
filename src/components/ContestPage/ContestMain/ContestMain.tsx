import React from 'react';
import s from './ContestMain.module.scss';
import ContestList from '../ContestList';
import ContestSwitcher from '../ContestSwitcher';

const ContestMain = () => {
  return (
    <div className={s.root}>
      <ContestSwitcher />
      <ContestList />
    </div>
  );
};

export default ContestMain;
