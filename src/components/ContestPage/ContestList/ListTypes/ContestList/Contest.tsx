import React from 'react';
import s from './Contest.module.scss';
import { useSelector } from 'store/store';
import { contestDataSelector } from 'store/selectors';

const Contest = () => {
  const contestData = useSelector(contestDataSelector);

  return <div>contest</div>;
};

export default Contest;
