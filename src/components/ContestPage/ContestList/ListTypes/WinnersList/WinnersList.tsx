import React from 'react';
import s from './WinnersList.module.scss';
import { useSelector } from 'store/store';
import { contestDataSelector } from 'store/selectors';
import ContestWinnerCard from 'components/cards/ContestWinnerCard';
import LoadMoreObserverWinners from 'components/ContestPage/LoadMore/Winners/Observer';

const WinnersList = () => {
  const contestData = useSelector(contestDataSelector);

  return (
    <div className={s.winnersList}>
      {contestData.map((item, id) => {
        return (
          <ContestWinnerCard
            key={id}
            id={item.id as number}
            title={item.title}
            image={item.image}
            firstPlacePrize={item.firstPlacePrize}
            countVotes={item.countVotes}
            winnerName={item.firstPlaceWinnerName}
            contestants={item.countContestants}
            finishedAt={item.finishedAt}
            startedAt={item.startedAt}
          />
        );
      })}
      <LoadMoreObserverWinners />
    </div>
  );
};

export default WinnersList;
