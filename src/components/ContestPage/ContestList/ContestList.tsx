import React, { useEffect, useState } from 'react';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { CircularProgress } from '@material-ui/core';
import s from './ContestList.module.scss';
import { useSelector } from 'store/store';
import { contestSelector } from 'store/selectors';
import Contest from './ListTypes/ContestList';
import WinnersList from './ListTypes/WinnersList';
import { useHistory } from 'react-router-dom';

const Preloader = () => {
  return (
    <div className={s.preload}>
      <CircularProgress size={55} />
    </div>
  );
};

const ContestList = () => {
  const [type, setType] = useState<string>('');

  const history = useHistory();
  const contest = useSelector(contestSelector);

  const { status, firstLoad } = contest;

  useEffect(() => {
    setType(history.location.search.slice(6));
  }, [history.location]);

  return (
    <>
      {status !== LoadingStatusEnum.SUCCESS && firstLoad ? (
        <Preloader />
      ) : (
        <div className={s.root}>
          {type !== 'winners' ? <Contest /> : <WinnersList />}
          {status === LoadingStatusEnum.LOADING ? <Preloader /> : null}
        </div>
      )}
    </>
  );
};

export default ContestList;
