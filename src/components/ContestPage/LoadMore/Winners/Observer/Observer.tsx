import React, { useEffect } from 'react';
import { contestSelector as _contestSelector } from 'store/selectors';
import { useAppDispatch, useSelector } from 'store/store';
import s from './Observer.module.scss';
import { useInView } from 'react-intersection-observer';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { LoadMoreWinners } from 'store/slices/contest/thunks';

const LoadMoreObserverWinners = () => {
  const dispatch = useAppDispatch();
  const contestSelector = useSelector(_contestSelector);

  const { status } = contestSelector;

  const { ref, inView } = useInView({
    threshold: 0,
  });

  useEffect(() => {
    if (status === LoadingStatusEnum.SUCCESS && inView) {
      dispatch(LoadMoreWinners({ page: contestSelector.pageToLoad }));
    }
  }, [inView]);

  return <div ref={ref} className={s.observerMarker}></div>;
};

export default LoadMoreObserverWinners;
