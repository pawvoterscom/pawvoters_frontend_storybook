import React from 'react';
import cn from 'classnames';
import { Typography } from '@material-ui/core';
import s from './StatItem.module.scss';

interface StatItemI {
  number: number;
  icon: React.ReactElement;
  type: 'likes' | 'position';
  size?: 'small' | 'regular';
  sufix?: boolean;
  className?: string;
}

const StatItem: React.FC<StatItemI> = ({
  icon,
  number,
  sufix = false,
  type,
  className,
  size = 'regular',
}) => {
  return (
    <div className={cn(s.root, className, s[type], s[size])}>
      <i>{icon}</i>
      <p>{number}</p>
      {sufix && <Typography variant="caption">nd</Typography>}
    </div>
  );
};

export default StatItem;
