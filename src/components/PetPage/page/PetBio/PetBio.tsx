import { Typography } from '@material-ui/core';
import React from 'react';
import { petDataSelector } from 'store/selectors';
import { useSelector } from 'store/store';
import s from './PetBio.module.scss';

export const PetBio = () => {
  const petData = useSelector(petDataSelector);

  return (
    <>
      {petData?.bio ? (
        <div className={s.root}>
          <Typography variant="h4" className={s.title}>
            Bio
          </Typography>
          {petData ? (
            <Typography variant="body1" className={s.info}>
              {petData?.bio}
            </Typography>
          ) : (
            <div className={s.infoPlaceholder}>
              <span></span>
              <span></span>
              <span></span>
            </div>
          )}
        </div>
      ) : null}
    </>
  );
};

export default PetBio;
