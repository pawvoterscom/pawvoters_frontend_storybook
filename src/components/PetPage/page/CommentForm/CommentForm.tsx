import { SendIcon } from 'components/icons';
import Avatar from 'components/ui/Avatar';
import { Input } from 'components/ui/Input';
import { RoundedButton } from 'components/ui/RoundedButton';
import React, { useState } from 'react';
import s from './CommentForm.module.scss';
import { useAppDispatch, useSelector } from 'store/store';
import { petDataSelector } from 'store/selectors';
import { setModalLoginOpened } from 'store/slices/app';
import User from 'lib/User';
import { createPostComment } from 'store/slices/pet/thunks';
import { createErrorToast } from 'lib/utils/toasts';

interface CommentFormProps {
  global?: boolean;
  parentId?: number;
}

const CommentForm: React.FC<CommentFormProps> = ({ global, parentId }) => {
  const petData = useSelector(petDataSelector);
  const dispatch = useAppDispatch();

  const [value, setValue] = useState('');
  const [isCommenting, setIsCommenting] = useState(false);

  const handleComment = async () => {
    if (!User.isUser()) return dispatch(setModalLoginOpened({ opened: true }));

    if (!global && petData && parentId) {
      setIsCommenting(true);

      const result = await dispatch(
        createPostComment({ postId: petData.id, text: value, parentId })
      );

      if (result.payload?.status !== 201)
        return createErrorToast({
          message: 'Something went wrong, please try again later',
        });

      if (result?.payload?.status === 201) {
        resetStates();
      }

      return;
    }

    if (petData) {
      setIsCommenting(true);

      dispatch(createPostComment({ postId: petData.id, text: value })).then(
        (result) => {
          if (result.payload?.status !== 201)
            return createErrorToast({
              message: 'Something went wrong, please try again later',
            });

          if (result?.payload?.status === 201) {
            resetStates();
          }
        }
      );
    }
  };

  const resetStates = () => {
    setValue('');
    setIsCommenting(false);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  return (
    <div className={s.addComment}>
      <div className={s.avatar}>
        <Avatar src={`${User.getUser().image}_48x48.jpg`} />
      </div>
      <div className={s.input}>
        <Input
          label="Add your comment"
          name="comment"
          multiline
          rows={5}
          className="comment"
          onChange={handleChange}
          value={value}
        />
      </div>
      <RoundedButton
        onClick={handleComment}
        disabled={Boolean(!value) || isCommenting}
        className={s.send}
      >
        <SendIcon />
      </RoundedButton>
    </div>
  );
};

export default CommentForm;
