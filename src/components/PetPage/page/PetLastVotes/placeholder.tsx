import s from './PetLastVotes.module.scss';

export const Placeholder = () => {
  return (
    <div className={s.avatarListPlaceholder}>
      <span />
      <span />
      <span />
    </div>
  );
};
