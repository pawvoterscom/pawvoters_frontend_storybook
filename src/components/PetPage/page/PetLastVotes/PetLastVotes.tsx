import React from 'react';
import { Typography } from '@material-ui/core';
import Avatar from 'components/ui/Avatar';
import s from './PetLastVotes.module.scss';
import { useSelector } from 'store/store';
import { petVotesSelector } from 'store/selectors';
import { LoadingStatusEnum } from '../../../../types/enum/LoadingStatus.enum';
import User from 'lib/User';
import { useUser } from 'lib/hooks/useUser';
import { Placeholder } from './placeholder';

const PetVotesFactory = () => {
  const petVotesData = useSelector(petVotesSelector);

  if (petVotesData.status !== LoadingStatusEnum.SUCCESS) {
    return <Placeholder />;
  }

  if (petVotesData.data.length === 0) {
    return (
      <div className={s.noVotes}>
        <Avatar src={`${User.getUser().image}_48x48.jpg`} />
        <Typography variant="body1" className={s.noVotesCaption}>
          Be the first!
        </Typography>
      </div>
    );
  }

  return (
    <div className={s.avatarList}>
      {petVotesData.data.map((user) => (
        <Avatar key={user.id} src={`${user.userImage}_48x48.jpg`} />
      ))}
    </div>
  );
};

const PetLastVotes = () => {
  const { isAuth } = useUser();

  return (
    <>
      {isAuth ? (
        <div className={s.root}>
          <Typography variant="caption" className={s.title}>
            Last votes
          </Typography>
          <PetVotesFactory />
        </div>
      ) : null}
    </>
  );
};

export default PetLastVotes;
