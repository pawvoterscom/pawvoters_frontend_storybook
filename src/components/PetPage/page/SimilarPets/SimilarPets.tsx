import Pet from 'components/cards/Pet';
import React, { useMemo, useRef } from 'react';
import StackGrid from 'react-stack-grid';
import s from './SimilarPets.module.scss';
import { useSelector } from 'store/store';
import { petSimilarSelector } from 'store/selectors';
import { useThrottledEvent } from 'lib/hooks/useThrottledEvent';
import useResizeObserver from 'use-resize-observer/polyfilled';

const SimilarPets = () => {
  const similarPetsData = useSelector(petSimilarSelector);
  const gridRef = useRef();
  useThrottledEvent({
    callback: () => {
      //@ts-ignore
      gridRef.current?.updateLayout();
    },
    event: 'resize',
    throttleTime: 1000,
  });
  const { ref, width = 1 } = useResizeObserver<HTMLDivElement>();

  const gridWith = useMemo(() => {
    return width < 480
      ? '100%'
      : width >= 480 && width < 740
      ? '50%'
      : width > 740
      ? '33.33%'
      : 284;
  }, [width]);

  return (
    <div className={s.root} ref={ref}>
      <StackGrid
        //@ts-ignore
        gridRef={(grid) => (gridRef.current = grid)}
        monitorImagesLoaded
        columnWidth={gridWith}
        gutterWidth={30}
        gutterHeight={30}
        appearDelay={0}
        duration={0}
      >
        {similarPetsData.data.map((item) => (
          <Pet key={item.id} {...item} />
        ))}
      </StackGrid>
    </div>
  );
};

export default SimilarPets;
