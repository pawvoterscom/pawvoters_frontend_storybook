import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import colors from 'theme/colors';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      padding: 0,
      '& li': {
        borderRadius: 5,
        height: 36,
        display: 'flex',
        alignItems: 'center',
        fontSize: 13,
        '&:hover': {
          background: 'rgba(235, 134, 75, 0.2)',
        },
      },
    },
    paper: {
      marginTop: 40,
      padding: 10,
      background: colors.bg1,
      boxShadow: '2px 10px 30px rgba(72, 103, 138, 0.12)',
      borderRadius: 5,
    },
  })
);
