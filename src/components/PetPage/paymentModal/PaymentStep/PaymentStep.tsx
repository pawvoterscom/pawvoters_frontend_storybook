import { Typography } from '@material-ui/core';
import { GoldCupIcon, PaymentAgregators, SearchIcon } from 'components/icons';
import Button from 'components/ui/Button';
import { Checkbox } from 'components/ui/Checkbox';
import React from 'react';
import { Link } from 'react-router-dom';
import s from './PaymentStep.module.scss';

const PaymentStep = () => {
  return (
    <div className={s.root}>
      <div className={s.note}>
        <GoldCupIcon />
        <Typography variant="body2">
          Help <span className={s.noteMarked}>Max</span> win the cintest by
          giving this pet <span className={s.noteMarked}>500 votes</span>
        </Typography>
      </div>
      <div className={s.creditCardForm}></div>
      <div className={s.saveCreditCard}>
        <Checkbox label="Save my credit card details" name="123" value={true} />
      </div>
      <Button className={s.startPayment}>
        <SearchIcon />
        Pay $5.49 for 500 votes
      </Button>
      <div className={s.footer}>
        <PaymentAgregators />
        <p>
          By clicking the button above you confirm to have accepted
          <Link to="/terms">Terms of Service</Link> and
          <Link to="/privacy">Privacy Policy</Link>
        </p>
      </div>
    </div>
  );
};

export default PaymentStep;
