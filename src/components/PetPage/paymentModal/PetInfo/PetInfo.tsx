import { Typography } from '@material-ui/core';
import { Like, PlaceIcon } from 'components/icons';
import StatItem from 'components/PetPage/common/StatItem';
import Image from 'components/ui/Image';
import React from 'react';
import { petDataSelector } from 'store/selectors';
import { useSelector } from 'store/store';
import s from './PetInfo.module.scss';

const PetInfo = () => {
  const petData = useSelector(petDataSelector);

  return (
    <div className={s.petInfo}>
      <div className={s.petImage}>
        <Image src={petData ? `${petData.image}_120x120.jpg` : ''} />
      </div>
      <div className={s.petInfoCol}>
        <Typography variant="overline" className={s.petBreed}>
          {petData?.breed.name}
        </Typography>
        <Typography variant="h4" className={s.petName}>
          {petData?.petName}
        </Typography>
        <div className={s.petStats}>
          <StatItem
            icon={<Like />}
            number={petData ? petData.countVotes : 0}
            type="likes"
            size="small"
          />
          <StatItem
            icon={<PlaceIcon />}
            number={petData ? petData.countVotes : 0}
            sufix
            type="position"
            size="small"
          />
        </div>
      </div>
    </div>
  );
};

export default PetInfo;
