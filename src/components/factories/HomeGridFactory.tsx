import React from 'react';
import { HomePost } from 'types/interfaces/HomePost.interface';
import { HomeCardsEnum } from '../../types/enum/HomeCards.enum';
import WinnerCard from '../cards/WinnerCard';
import avatar from '../../static/images/development/winners_card_avatar.jpg';
import winnerImage from '../../static/images/development/winnders_card.jpg';
import AddPet from '../cards/AddPet';
import LastVotes from '../cards/LastVotes';
import Pet from '../cards/Pet';

const winnersItems = [
  {
    id: 0,
    owner: {
      avatar: avatar,
      comment:
        'A fun competition where we can show off our lovely pets with a lovely bonus if we win - THANK YOU!',
      ownerName: 'Hazel L.',
    },
    pet: {
      image: winnerImage,
      petName: 'Luna1',
      wonAmount: 200,
    },
  },
  {
    id: 1,
    owner: {
      avatar: avatar,
      comment:
        'A fun competition where we can show off our lovely pets with a lovely bonus if we win - THANK YOU!',
      ownerName: 'Hazel L.',
    },
    pet: {
      image: winnerImage,
      petName: 'Luna2',
      wonAmount: 200,
    },
  },
  {
    id: 2,
    owner: {
      avatar: avatar,
      comment:
        'A fun competition where we can show off our lovely pets with a lovely bonus if we win - THANK YOU!',
      ownerName: 'Hazel L.',
    },
    pet: {
      image: winnerImage,
      petName: 'Luna3',
      wonAmount: 200,
    },
  },
];

const HomeGridFactory = ({ component }: { component: HomePost }) => {
  switch (component.type) {
    case HomeCardsEnum.WINNER_CARD:
      return <WinnerCard key={component.id} items={winnersItems} />;
    case HomeCardsEnum.ADD_PET:
      return <AddPet key={component.id} />;
    case HomeCardsEnum.LAST_VOTES:
      return <LastVotes key={component.id} />;
    default:
      return (
        <Pet
          key={component.id}
          id={component.id}
          image={component.image}
          userNickname={component.userNickname}
          petName={component.petName}
          countVotes={component.countVotes}
          backgroundColor={component.backgroundColor}
          originalImageHeight={component.originalImageHeight}
          originalImageWidth={component.originalImageWidth}
        />
      );
  }
};

export default HomeGridFactory;
