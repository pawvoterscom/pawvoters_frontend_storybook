import { CircularProgress } from '@material-ui/core';
import React from 'react';
import { homePetsSelector } from 'store/selectors';
import { useSelector } from 'store/store';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import s from './HomeLoadingState.module.scss';

const HomeLoadingState = () => {
  const pets = useSelector(homePetsSelector);

  return (
    <>
      {pets.status === LoadingStatusEnum.LOADING && pets.firstLoad && (
        <div className={s.loadingState}>
          <CircularProgress size={48} />
        </div>
      )}
    </>
  );
};

export default HomeLoadingState;
