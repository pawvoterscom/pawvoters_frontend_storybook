import React, { FC } from 'react';
import { Helmet } from 'react-helmet';

interface Props {
  title?: string;
  description?: string;
  image?: string;
  url?: string;
}

const Head: FC<Props> = ({
  title = 'Pawvoters | Best voting platform',
  description = 'Best description for pawvoters',
  image = 'https://picsum.photos/600/500',
  url = 'https://pawvoters.com',
}) => {
  return (
    <Helmet>
      <meta property="og:title" content={title} />
      <meta property="og:url" content={url} />
      <meta property="og:image" content={image} />
      <meta property="og:description" content={description} />
      <meta property="og:type" content="article" />
      <meta name="twitter:card" content={image} />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
      <title>{title}</title>
      <meta name="description" content={description} />
    </Helmet>
  );
};

export default Head;
