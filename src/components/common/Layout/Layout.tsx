import React, { FC } from 'react';
import s from './Layout.module.scss';
import cn from 'classnames';
import Header from '../Header';
import Footer from '../Footer';
import MobileNav from '../MobileNav';
import SearchButton from '../../ui/SearchButton';

interface Props {
  needHeaderFooter?: boolean;
  children: React.ReactNode;
}

const Layout: FC<Props> = ({ needHeaderFooter, children }) => {
  return (
    <div className={cn(s.wrapper)}>
      {needHeaderFooter ? (
        <>
          <Header className={cn(s.header)} />
          <main className={cn(s.main)}>{children}</main>
          <Footer className={cn(s.footer)} />
          <MobileNav />
          <SearchButton className={cn(s.searchButton)} />
        </>
      ) : (
        <main className={cn(s.main)}>{children}</main>
      )}
    </div>
  );
};

export default Layout;
