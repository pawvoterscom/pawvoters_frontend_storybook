import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import User, { UserStatuses } from 'lib/User';

const PrivateRoute = ({
  component,
  path,
  ...rest
}: {
  component: any;
  path: string;
}) => {
  return (
    <Route
      path={path}
      {...rest}
      render={() =>
        User.getUser() !== UserStatuses.NO_USER ? (
          component
        ) : (
          <Redirect to="/" />
        )
      }
    />
  );
};

export default PrivateRoute;
