import React, { useEffect } from 'react';
import s from './NotificationMain.module.scss';
import { CircularProgress } from '@material-ui/core';
import { useSelector, useAppDispatch } from 'store/store';
import {
  notificationDataSelector,
  notificationSelector,
} from 'store/selectors/user';
import NotificationList from '../NotificationList';
import CloseIcon from '../../../icons/Close';
import { resetNotifications, setModalOpened } from 'store/slices/user';

const Notification = () => {
  const notification = useSelector(notificationSelector);
  const notificationData = useSelector(notificationDataSelector);

  const dispatch = useAppDispatch();

  const handleClick = () => dispatch(setModalOpened(false));

  useEffect(() => {
    return () => {
      dispatch(resetNotifications());
    };
  }, [dispatch]);

  return (
    <div className={s.notificationMenu}>
      <div className={s.wrapper}>
        {notificationData.length > 0 && (
          <>
            <div className={s.closeButton} onClick={handleClick}>
              <CloseIcon />
            </div>
            <div className={s.notificationCount}>
              You have {notification.unread} new notifications
            </div>
          </>
        )}
        <NotificationList />
        {!notification.firstLoad && notificationData.length === 0 && (
          <div className={s.firstLoad}>
            <CircularProgress size={40} />
          </div>
        )}
      </div>
    </div>
  );
};

export default Notification;
