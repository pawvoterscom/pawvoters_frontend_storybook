import React from 'react';
import s from './NotificationContent.module.scss';
import { UserNotification } from 'types/user/UserNotification.type';

interface DataType {
  data: UserNotification[];
}

const NotificationContent: React.FC<DataType> = ({ data }) => {
  return <div className={s.content}>1</div>;
};

export default NotificationContent;
