import React, { FC } from 'react';
import cn from 'classnames';
import { Container } from '@material-ui/core';
import s from './Footer.module.scss';
import { Link } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import { LogoIcon } from '../../icons';
import FooterPets from '../../icons/footerPets';
import ScrollTopButton from "../../ui/ScrollTopButton";

const navigationsItems = [
  {
    id: 0,
    label: 'About Us',
    to: '/about',
  },
  {
    id: 1,
    label: 'Leaderboard',
    to: '/leaderboard',
  },
  {
    id: 2,
    label: 'Cat contest',
    to: '/cats',
  },
  {
    id: 3,
    label: 'Dog contest',
    to: '/dogs',
  },

  {
    id: 4,
    label: 'Winners',
    to: '/winners',
  },

  {
    id: 5,
    label: 'Privaci Policy (GDPR)',
    to: '/privacy',
  },

  {
    id: 6,
    label: 'Terms & conditions',
    to: '/terms',
  },

  {
    id: 7,
    label: 'Help',
    to: '/help',
  },
];

interface Props {
  className: string;
}

const Footer: FC<Props> = ({ className }) => {
  return (
    <footer className={cn(className)}>
      <Container className={s.container}>
        <div className={cn(s.root)}>
          <div className={cn(s.wrap)}>
            <Link to="/" className={cn(s.logo)}>
              <LogoIcon />
              <span>Pawvoters</span>
            </Link>
            <nav className={cn(s.navigation)}>
              
              {navigationsItems.map((item) => (
                <Link key={item.id} to={item.to}>
                  <Typography variant="body1">{item.label}</Typography>
                </Link>
              ))}
            </nav>
            <div className={cn(s.footerPets)}>
              <FooterPets />
            </div>
          </div>
          <div className={cn(s.text)}>
            <Typography variant="caption">© 2020 pawvoters.com</Typography>
            <Typography variant="caption">
              Created by the best people in the world
            </Typography>
          </div>
        </div>
        <ScrollTopButton className={s.toTopButton} />
      </Container>
    </footer>
  );
};

export default Footer;
