import React from 'react';
import cn from 'classnames';
import s from './Navigation.module.scss';
import { Link, useHistory } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import { useAppDispatch } from 'store/store';
import { setModalLoginOpened } from 'store/slices/app';
import User, { UserStatuses } from 'lib/User';

// Если доступ только после входа в аккаунт, то ставим private: true
const navigationsItems = [
  {
    id: 0,
    label: 'Contest',
    private: false,
    to: '/contest',
  },
  {
    id: 1,
    label: 'Leaderboard',
    private: false,
    to: '/leaderboard',
  },
  {
    id: 2,
    label: 'My pets',
    private: true,
    to: '/pets',
  },
];

const Navigation = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleClick = (item_private: boolean) => {
    if (User.getUser() === UserStatuses.NO_USER && item_private === true)
      dispatch(setModalLoginOpened({ opened: true }));
  };

  return (
    <nav className={cn(s.navigation)}>
      {navigationsItems.map((item) => (
        <Link
          to={
            item.private !== false && User.getUser() === UserStatuses.NO_USER
              ? history.location.pathname
              : item.to
          }
          key={item.id}
          onClick={() => handleClick(item.private)}
        >
          <Typography variant="body1">{item.label}</Typography>
        </Link>
      ))}
    </nav>
  );
};

export default Navigation;
