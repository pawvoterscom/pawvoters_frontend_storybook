import React, { useEffect, useRef, useState } from 'react';
import cn from 'classnames';
import s from './UserBlock.module.scss';
import { Add, LoginSignupIcon, NotificationIcon } from '../../icons';
import Button from '../../ui/Button';
import { RoundedButton } from '../../ui/RoundedButton';
import { Link, useHistory } from 'react-router-dom';
import { setModalLoginOpened } from 'store/slices/app';
import { useAppDispatch, useSelector } from 'store/store';
import { transformName } from '../../../lib/utils/string';
import { useUser } from 'lib/hooks/useUser';
import {
  getNotifications,
  getUnreadNotifications,
} from 'store/slices/user/thunks';
import { notificationSelector } from 'store/selectors/user';
import Notification from '../Notification/NotificationMain';
import { setModalOpened } from 'store/slices/user';
import { readNotifications } from 'lib/utils/read-notifications';
import Avatar from 'components/ui/Avatar';
import User from 'lib/User';
import Paw from 'components/icons/Paw';
import MyAccount from 'components/icons/MyAccount';
import SignOut from 'components/icons/SignOut';
import useOnClickOutside from 'lib/hooks/useOnClickOutside';

const UserBlock = () => {
  const [dropDownStatus, setDropDownStatus] = useState<boolean>(false);

  const dispatch = useAppDispatch();
  const history = useHistory();

  const modalRef = useRef<HTMLDivElement>(null);
  const dropRef = useRef<HTMLDivElement>(null);

  const notifications = useSelector(notificationSelector);

  const { isAuth, userData } = useUser();

  const { pageToLoad, opened } = notifications;

  useEffect(() => {
    if (isAuth) dispatch(getUnreadNotifications({}));
  }, [isAuth]);

  useEffect(() => {
    if (!opened && isAuth) dispatch(readNotifications(notifications.data));
  }, [opened]);

  const handleButtonClick = (e: any) => {
    e.preventDefault();

    if (!isAuth) {
      return dispatch(setModalLoginOpened({ opened: true }));
    }

    history.push('/add');
  };

  const handleModal = () => {
    if (!opened) dispatch(getNotifications({ page: pageToLoad }));

    dispatch(setModalOpened(!opened));
  };

  const modalClickOutside = () => dispatch(setModalOpened(false));
  const dropClickOutside = () => setDropDownStatus(false);

  useOnClickOutside(modalRef, modalClickOutside, opened, true);
  useOnClickOutside(dropRef, dropClickOutside, dropDownStatus);

  const handleUser = () => setDropDownStatus(!dropDownStatus);

  return (
    <div className={cn(s.userBlock)}>
      <Link to="/add">
        <Button className={cn(s.addPetButton)} color="secondary">
          <span>
            <Add />
            Add pet
          </span>
        </Button>
      </Link>
      <RoundedButton
        className={cn(s.addPetButtonMobile)}
        onClick={handleButtonClick}
      >
        <Add />
      </RoundedButton>
      {isAuth ? (
        <div className={s.authUser}>
          <div
            ref={modalRef}
            className={cn(s.notificationBlock, {
              [s.active]: opened,
            })}
          >
            <button onClick={handleModal} className={s.notifications}>
              <NotificationIcon />
              {!notifications.unread ? null : <span></span>}
            </button>
            {opened && <Notification />}
          </div>
          <div
            className={cn(s.user, {
              [s.active]: dropDownStatus,
            })}
            onClick={handleUser}
            ref={dropRef}
          >
            <Avatar
              className={s.userAvatar}
              src={`${userData?.image}_48x48.jpg`}
            />
            <p className={s.userName}>{transformName(userData?.nickname)}</p>
            {dropDownStatus ? (
              <ul className={s.dropDown}>
                <li onClick={() => history.push('/add')}>
                  Add pet
                  <i className={cn(s.icon, s.invertedIcon)}>
                    <Paw />
                  </i>
                </li>
                <li onClick={() => history.push('/pets')}>
                  My pet
                  <i className={s.icon}>
                    <Paw />
                  </i>
                </li>
                <li onClick={() => history.push('/me')}>
                  My account
                  <i className={s.icon}>
                    <MyAccount />
                  </i>
                </li>
                <li onClick={() => User.logout()}>
                  Sign out
                  <i className={s.icon}>
                    <SignOut />
                  </i>
                </li>
              </ul>
            ) : null}
          </div>
        </div>
      ) : (
        <button
          className={cn(s.loginLink)}
          type="button"
          onClick={handleButtonClick}
        >
          <span className={s.loginIcon}>
            <LoginSignupIcon />
          </span>
          <span>Login / Sign up</span>
        </button>
      )}
    </div>
  );
};

export default UserBlock;
