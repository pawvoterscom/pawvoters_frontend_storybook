import React, { useState, ChangeEvent, SyntheticEvent, useEffect } from 'react';
import s from './AddMedia.module.scss';
import PetsAdd from '../../icons/PetsAdd';
import { Typography } from '@material-ui/core';
import { formatBytes } from 'lib/utils/format_bytes';
import { createErrorToast } from 'lib/utils/toasts';
import { EditIcon } from 'components/icons';

interface DragWrapperType extends ImageType {
  maxFileSize: number;
  format?: string[];
  defaultFormat?: boolean;
  handleChangeFile?: (file: File) => void;
  editMedia?: boolean;
}

export interface DOMEvent<T extends EventTarget> extends Event {
  target: T;
  currentTarget: T;
}

interface AddMedia extends DragWrapperType {}

type FileHandlerType = {
  size: number;
  type: string;
};

type MediaSize = {
  naturalHeight: number;
  naturalWidth: number;
};

type ImageType = {
  src?: string;
};

export class Media {
  private static file: File;
  private static size: MediaSize;

  // Получаем файл и инициализируем его
  public static setFile(file: File) {
    this.file = file;
  }

  // Отдаем инициализированный файл
  public static getFile() {
    return this.file;
  }

  // Получаем оригинальную ширину/высоту и инициализируем
  public static setSize(naturalHeight: number, naturalWidth: number) {
    this.size = { naturalHeight, naturalWidth };
  }

  // Отдаем инициализированный размер
  public static getSize() {
    return this.size;
  }
}

const Image: React.FC<ImageType> = ({ src }) => {
  const handleLoad = (
    e: DOMEvent<HTMLImageElement> & SyntheticEvent<HTMLImageElement>
  ) => {
    const { naturalHeight, naturalWidth } = e.target;

    Media.setSize(naturalHeight, naturalWidth);
  };

  return <img className={s.mediaImage} src={src} onLoad={handleLoad} />;
};

const defaultSrcState: string = '';

const DragWrapper: React.FC<DragWrapperType> = ({
  editMedia,
  maxFileSize,
  format,
  defaultFormat,
  src,
  handleChangeFile,
}) => {
  const [_src, setSrc] = useState<string>(defaultSrcState);
  const [_statusDrag, setStatusDrag] = useState<boolean>(false);

  const formatedSize: string = formatBytes(maxFileSize, 0);

  useEffect(() => {
    return () => {
      setSrc(defaultSrcState);
    };
  }, []);

  useEffect(() => {
    setSrc(src ? src : '');
  }, [src]);

  /**
   * Проверяем формат файла с форматами, которые указаны в AddMedia,
   * если форматы схожи, то пропускаем проверку и успешно выполняем загрузку файла.
   * @param _format - формат файла.
   * @param _defaultFormat - дефолтные форматы файла. В него входят такие форматы, как: png | jpg | jpeg
   */
  const formatChecker = (_format: string, _defaultFormat?: boolean) => {
    let types: string[] = [];
    const dF: string[] = ['image/png', 'image/jpeg', 'image/jpg'];

    if (_defaultFormat) types.push(...dF);

    if (format)
      for (let i = 0; i < format.length; i++) types.push(`image/${format[i]}`);

    const result = (types: string) => _format === types;

    return !types.some(result);
  };

  const fileHandler = ({ size, type }: FileHandlerType) => {
    type FilterType = {
      exceededSize: boolean;
      _format: boolean;
    };

    const filter: FilterType = {
      exceededSize: size >= maxFileSize,
      _format: formatChecker(type, defaultFormat),
    };

    const { exceededSize, _format } = filter;

    if (exceededSize || _format) {
      if (exceededSize)
        createErrorToast({ message: `Exceeded size, max ${formatedSize}` });

      if (_format) createErrorToast({ message: 'Unsupported Format' });

      return false;
    }

    return true;
  };

  const uploadHandler = (e: DOMEvent<HTMLInputElement> & ChangeEvent): void => {
    if (e.currentTarget.files) {
      const reader: FileReader = new FileReader();

      const _file: File = e.currentTarget.files[0];
      const { size, type }: FileHandlerType = _file;

      const allowed: boolean = fileHandler({ size, type });

      if (allowed) {
        reader.onload = (e: ProgressEvent<FileReader>): void => {
          if (e.target) return setSrc(e.target.result as string);
        };

        if (handleChangeFile && _file) handleChangeFile(_file);

        Media.setFile(_file);

        reader.readAsDataURL(_file);
      }
    }
  };

  const withPrevent = (_function: any) => {
    setStatusDrag(!_statusDrag);

    return (e: React.DragEvent<HTMLDivElement>): void => {
      e.preventDefault();
      _function(e);
    };
  };

  const dropZone = (
    <input className={s.inputZone} type="file" onChange={uploadHandler} />
  );

  if (_src)
    return (
      <>
        {editMedia ? (
          <div className={s.editMedia}>
            {dropZone}
            <EditIcon />
          </div>
        ) : null}
        <Image src={_src} />
      </>
    );

  return (
    <div className={s.mediaWrapper}>
      <div
        className={s.dragWrapper}
        onDragEnter={withPrevent}
        onDragLeave={withPrevent}
      >
        {dropZone}
        <PetsAdd />
        <Typography variant="h4">
          {_statusDrag ? 'Drop' : `Pet's photo & video`}
        </Typography>
        <Typography variant="overline">
          Minimum size 640x640px, max 5MB, jpg, png
        </Typography>
      </div>
    </div>
  );
};

const AddMedia: React.FC<AddMedia> = ({
  src,
  editMedia,
  maxFileSize,
  format,
  defaultFormat,
  handleChangeFile,
}) => {
  return (
    <div className={s.addMedia}>
      <DragWrapper
        src={src as string}
        defaultFormat={defaultFormat}
        editMedia={editMedia}
        maxFileSize={maxFileSize}
        format={format}
        handleChangeFile={handleChangeFile}
      />
    </div>
  );
};

export default AddMedia;
