import React from 'react';
import s from './MobileNav.module.scss';
import { NavLink, useHistory } from 'react-router-dom';
import { RoundedButton } from '../../ui/RoundedButton';
import cn from 'classnames';
import { Add, Contest, Home, Leaderboard, MyPets } from '../../icons';
import { Typography } from '@material-ui/core';
import User, { UserStatuses } from 'lib/User';
import { useAppDispatch } from 'store/store';
import { setModalLoginOpened } from 'store/slices/app';

const MobileNav = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleClick = () => {
    if (User.getUser() === UserStatuses.NO_USER)
      dispatch(setModalLoginOpened({ opened: true }));
  };

  const handleRedirect = (to: string) => {
    return User.getUser() === UserStatuses.NO_USER
      ? history.location.pathname
      : to;
  };

  const handleActive = (path: string) => {
    const pathname: string = history.location.pathname;

    if (pathname !== path) return '';

    return s.active;
  };

  return (
    <nav className={s.root}>
      <NavLink to="/" activeClassName={s.active} className={s.navItem} exact>
        <Home />
        <Typography variant="overline">Home</Typography>
      </NavLink>
      <NavLink to="/contest" activeClassName={s.active} className={s.navItem}>
        <Contest />
        <Typography variant="overline">Contest</Typography>
      </NavLink>
      <NavLink
        to={handleRedirect('/add')}
        activeClassName={handleActive('/add')}
        onClick={handleClick}
        className={s.navItem}
      >
        <RoundedButton className={cn(s.addPet)}>
          <Add />
        </RoundedButton>
      </NavLink>
      <NavLink
        to="/leaderboard"
        activeClassName={s.active}
        className={s.navItem}
      >
        <Leaderboard />
        <Typography variant="overline">Leaderboard</Typography>
      </NavLink>
      <NavLink
        to={handleRedirect('/pets')}
        activeClassName={handleActive('/pets')}
        onClick={handleClick}
        className={s.navItem}
      >
        <MyPets />
        <Typography variant="overline">My pets</Typography>
      </NavLink>
    </nav>
  );
};

export default MobileNav;
