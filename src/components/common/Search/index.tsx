import classNames from 'classnames';
import React, { useState } from 'react';
import styles from './index.module.scss';
import { ClearIcon, SearchIcon } from '../../icons';
import { useHistory } from 'react-router';
import { useAppDispatch, useSelector } from 'store/store';
import { getPosts, getPostsAutoComplete } from 'store/slices/search/thunks';
import {
  changeQuery,
  changeHeaderQuery,
  clearHeaderQuery,
  changeActualQuery,
  setAutoCompleteModal,
  resetAutoCompleteData,
} from 'store/slices/search';
import { searchSelector } from 'store/selectors';

export interface SearchProps {
  placeholder: string;
  myref: any;
}

export const Search: React.FC<SearchProps> = ({ placeholder, myref }) => {
  const [focused, setFocused] = useState<boolean>(false);
  const posts = useSelector(searchSelector);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;

    dispatch(changeHeaderQuery(value));

    if (value.length >= 2) {
      dispatch(getPostsAutoComplete({ query: value, per_page: 3 }));
      dispatch(setAutoCompleteModal(true));

      document.addEventListener('click', handleOutsideClick, false);
    } else {
      document.removeEventListener('click', handleOutsideClick, false);
      dispatch(setAutoCompleteModal(false));
    }
  };

  const handleOutsideClick = (e: any) => {
    if (!myref.current?.contains(e.target)) {
      dispatch(resetAutoCompleteData());
    }
  };

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  const handleClearInput = () => {
    dispatch(clearHeaderQuery());
    dispatch(resetAutoCompleteData());
  };

  const handleEnter = (e: any) => {
    if (e.key === 'Enter') {
      dispatch(changeQuery(posts.value.headerQuery));
      dispatch(changeActualQuery(posts.value.headerQuery));

      if (posts.value.headerQuery) {
        dispatch(resetAutoCompleteData());
        dispatch(getPosts({ query: posts.value.headerQuery }));
        dispatch(changeHeaderQuery(''));
        history.push(`/search/posts?query=${posts.value.headerQuery}`);
      }
    }
  };

  return (
    <div
      className={classNames(styles.searchWrapper, {
        [styles.searchFocused]: focused,
      })}
    >
      <input
        type="text"
        placeholder={placeholder}
        onChange={handleChange}
        value={posts.value.headerQuery}
        className={styles.searchInput}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onKeyDown={handleEnter}
      />
      <i className={styles.actionIcon}>
        {!posts.value.headerQuery ? (
          <button
            className={styles.searchIcon}
            onClick={() => history.push('/search')}
          >
            <SearchIcon />
          </button>
        ) : (
          <button className={styles.clearButton} onClick={handleClearInput}>
            <ClearIcon />
          </button>
        )}
      </i>
    </div>
  );
};
