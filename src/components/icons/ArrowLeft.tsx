import React from 'react';

const ArrowLeft = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M11.05 12.364L16 7.414L14.586 6L8.222 12.364L14.586 18.728L16 17.314L11.05 12.364Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default ArrowLeft;
