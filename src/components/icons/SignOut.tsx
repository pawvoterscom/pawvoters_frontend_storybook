import React from 'react';

const SignOut = () => {
  return (
    <svg
      viewBox="0 0 24 24"
      width="24"
      height="24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <defs>
        <clipPath id="br55a-90">
          <path d="M18 2v14c0 1.1-.9 2-2 2H2c-1.1 0-2-.9-2-2v-3c0-.55.45-1 1-1s1 .45 1 1v2c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1H3c-.55 0-1 .45-1 1v2c0 .55-.45 1-1 1s-1-.45-1-1V2a2 2 0 0 1 2-2h14c1.1 0 2 .9 2 2zM7.79 11.88L9.67 10H1c-.55 0-1-.45-1-1s.45-1 1-1h8.67L7.79 6.11A.996.996 0 1 1 9.2 4.7l3.59 3.59c.39.39.39 1.02 0 1.41L9.2 13.29a.996.996 0 1 1-1.41-1.41z"></path>
        </clipPath>
      </defs>
      <g>
        <g>
          <g></g>
          <g clipPath="url(#br55a-90)">
            <g>
              <path d="M-3-3h24v24H-3z"></path>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default SignOut;
