import React from 'react';

const Month = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
  >
    <defs>
      <clipPath id="month-1">
        <path d="M16 17V7H2v10c0 .55.45 1 1 1h12c.55 0 1-.45 1-1zM14 0c.55 0 1 .45 1 1v1h1c1.1 0 2 .9 2 2v14c0 1.1-.9 2-2 2H2a2 2 0 0 1-2-2L.01 4C.01 2.9.89 2 2 2h1V1c0-.55.45-1 1-1s1 .45 1 1v1h8V1c0-.55.45-1 1-1zm0 12v3c0 .55-.45 1-1 1h-3c-.55 0-1-.45-1-1v-3c0-.55.45-1 1-1h3c.55 0 1 .45 1 1z"></path>
      </clipPath>
    </defs>
    <g>
      <g>
        <g></g>
        <g clipPath="url(#month-1)">
          <g>
            <path fillOpacity=".32" d="M-3-2h24v24H-3z"></path>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default Month;
