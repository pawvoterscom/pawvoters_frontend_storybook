import React, { useEffect } from 'react';
import s from './LastVotes.module.scss';
import cn from 'classnames';
import { Typography } from '@material-ui/core';
import { useAppDispatch, useSelector } from 'store/store';
import { lastVotesSelector } from 'store/selectors';
import { fetchLastVotes } from 'store/slices/home/thunks';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import formatDistanceStrict from 'date-fns/formatDistanceStrict';
import Avatar from 'components/ui/Avatar';
import { generatePetSlug } from 'lib/utils/url-utils';
import { useHistory } from 'react-router';
import { CircularProgress } from '@material-ui/core';

interface ILastVoteItem {
  avatar: string;
  name: string;
  petName: string;
  postId: number;
  votedFor: string;
  timestamp: string;
}

const LastVoteItem: React.FC<ILastVoteItem> = ({
  avatar,
  name,
  petName,
  postId,
  timestamp,
  votedFor,
}) => {
  const history = useHistory();

  return (
    <div
      className={s.lastVoteRoot}
      onClick={() => history.push(`/post/${generatePetSlug(petName, postId)}`)}
    >
      <div className={s.userAvatar}>
        <Avatar src={avatar} alt={name} />
      </div>
      <div className={s.userInfo}>
        <Typography variant="body1">{name}</Typography>
        <Typography variant="caption">Voted for {votedFor}</Typography>
      </div>
      <div className={s.timestamp}>
        <Typography variant="overline">
          {formatDistanceStrict(new Date(timestamp), new Date(), {
            addSuffix: false,
          })
            .replace(/\s+/g, '')
            .match(RegExp(/^\d+./))}
        </Typography>
      </div>
    </div>
  );
};

const LastVotes = () => {
  const dispatch = useAppDispatch();
  const lastVotes = useSelector(lastVotesSelector);

  useEffect(() => {
    if (lastVotes.status === LoadingStatusEnum.SUCCESS) {
      return;
    }

    dispatch(fetchLastVotes());
  }, []);

  return (
    <div className={cn(s.root)}>
      {lastVotes.status === LoadingStatusEnum.SUCCESS ? (
        lastVotes.data.map((item) => (
          <LastVoteItem
            key={item.id}
            postId={item.postId}
            petName={item.postPetName}
            avatar={item.userImage}
            name={item.userNickname}
            timestamp={item.created}
            votedFor={item.postPetName}
          />
        ))
      ) : (
        <div className={s.preload}>
          <CircularProgress size={55} />
        </div>
      )}
    </div>
  );
};

export default LastVotes;
