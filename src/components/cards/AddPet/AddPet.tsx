import { Typography } from '@material-ui/core';
import cn from 'classnames';
import React, { useEffect, useState } from 'react';
import { Add } from '../../icons';
import Button from '../../ui/Button';
import { Form, Formik } from 'formik';
import s from './AddPet.module.scss';
import AddMedia from '../../common/AddMedia';
import { useHistory } from 'react-router-dom';
import FormikInput from 'components/ui/FormikInput';
import * as Yup from 'yup';
import { shallowEqual } from 'react-redux';
import User, { UserStatuses } from 'lib/User';
import { useAppDispatch } from 'store/store';
import { setModalLoginOpened } from 'store/slices/app';

type initialValuesType = {
  petName: string;
};

const AddPet = () => {
  const history = useHistory();
  const dispatch = useAppDispatch();

  const [file, setFile] = useState<File | string>('');

  const [initialValues, setInitialValues] = useState<initialValuesType>({
    petName: '',
  });

  useEffect(() => {
    setInitialValues((values) => ({
      ...values,
    }));
  }, []);

  const validationSchema = Yup.object({
    petName: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(60, 'Maximum 60 symbols')
      .required('Required field'),
  });

  const getBASE64 = (_file: File) => {
    return new Promise((res, rej) => {
      const reader: FileReader = new FileReader();

      reader.onload = () => res(reader.result);

      reader.onerror = (e: any) => rej(e);

      reader.readAsDataURL(_file);
    });
  };

  const handleChangeFile = (file: File) => {
    setFile(file);
  };

  const handleSubmit = async (values: initialValuesType) => {
    if (User.getUser() !== UserStatuses.NO_USER) {
      const result = await getBASE64(file as File);

      const params = {
        ...values,
        image: result,
      };

      localStorage.setItem('pet', JSON.stringify(params));

      return history.push('/add');
    }

    dispatch(setModalLoginOpened({ opened: true }));
  };

  return (
    <div className={cn(s.cardWrapper)}>
      <AddMedia
        maxFileSize={5242880}
        editMedia={true}
        defaultFormat
        handleChangeFile={handleChangeFile}
      />
      <div className={s.addPetForm}>
        <Formik
          enableReinitialize
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          {({ values, initialValues, isValid, isSubmitting }) => (
            <Form>
              <Typography variant="h3">Add Your Pet</Typography>
              <div className={s.bottomed}>
                <FormikInput
                  label="Pet Name *"
                  name="petName"
                  beginIcon={<Add color="#A4AFBE" />}
                />
              </div>
              <Button
                type="submit"
                disabled={
                  shallowEqual(values, initialValues) ||
                  !isValid ||
                  isSubmitting
                }
              >
                <span>
                  <Add />
                  Add Pet
                </span>
              </Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default AddPet;
