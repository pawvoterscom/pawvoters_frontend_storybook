import React from 'react';
import s from './ContestWinnerCard.module.scss';
import cn from 'classnames';
import Button from 'components/ui/Button';
import { Typography } from '@material-ui/core';
import { format } from 'date-fns';
import Month from 'components/icons/Month';
import Contestants from 'components/icons/Contestants';
import { useHistory } from 'react-router-dom';

export interface ContestWinnerCardProps {
  id: number;
  title: string;
  image: string;
  firstPlacePrize: number | string;
  countVotes: number | string;
  winnerName: string;
  contestants: string | number;
  finishedAt: string;
  startedAt: string;
}

const ContestWinnerCard: React.FC<ContestWinnerCardProps> = ({
  title,
  image,
  firstPlacePrize,
  countVotes,
  winnerName,
  contestants,
  finishedAt,
  startedAt,
  id,
}) => {
  const history = useHistory();

  const handleClick = (contestId: number) => {
    history.push(`/leaderboard?type=winners&contestId=${contestId}`);
  };

  return (
    <div className={s.winnerCard}>
      <div className={s.winnerCardImage}>
        <img src={`${image}_600x600.jpg`} />
      </div>
      <div className={s.winnerCardContent}>
        <div className={s.contestHeader}>
          <Typography className={s.title} variant="h4">
            {title}
          </Typography>
          <div className={cn(s.placePrize, s.placePrizeDesktop)}>
            1st place prize: {firstPlacePrize}
          </div>
        </div>
        <div className={s.contestSubHeader}>
          <span>1 st - {winnerName}</span>
          <span> | </span>
          <span className={s.votes}>{countVotes} votes</span>
        </div>
        <div className={cn(s.placePrize, s.placePrizeMobile)}>
          1st place prize: {firstPlacePrize}
        </div>
        <div className={s.contestBottom}>
          <div className={s.time}>
            <div className={s.monthIcon}>
              <Month />
            </div>
            <span>{format(new Date(startedAt), 'LLLL dd')}</span>
            <span className={s.divis}>-</span>
            <span>{format(new Date(finishedAt), 'LLLL dd, yyyy')}</span>
          </div>
          <div className={s.contestants}>
            <div className={s.contestantsIcon}>
              <Contestants />
            </div>
            {contestants} Contestants
          </div>
          <div className={s.btnBlock}>
            <Button onClick={() => handleClick(id)}>Leaderboard</Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContestWinnerCard;
