import React from 'react';
import s from './MePageLinked.module.scss';
import Avatar from '../../ui/Avatar';
import { Typography } from '@material-ui/core';

const MePageLinked = () => (
  <div className={s.root}>
    <div className={s.wrapper}>
      <Avatar
        className={s.avatar}
        src="https://images.unsplash.com/photo-1611616327270-91f4abdfcb03?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80"
      />
      <Typography variant="body2" className={s.linkedType}>
        Linked Facebook account
      </Typography>
    </div>
    <Typography variant="overline" className={s.note}>
      You already have a Facebook account linked. Since this account is your
      primary mode of connection, you can’t change it.
    </Typography>
  </div>
);

export default MePageLinked;
