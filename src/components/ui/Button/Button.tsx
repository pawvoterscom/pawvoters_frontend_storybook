import { Button as MaterialButton } from '@material-ui/core';
import React from 'react';
import { useButtonStyles } from './styles';

export interface ButtonProps {
  children: React.ReactNode;
  disabled?: boolean;
  variant?: 'contained' | 'outlined';
  color?: 'primary' | 'secondary';
  onClick?:
    | ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)
    | undefined;
  className?: string;
  href?: string;
  size?: 'small' | 'medium' | 'large';
  type?: 'button' | 'submit';
}

const Button: React.FC<
  ButtonProps & React.HTMLAttributes<HTMLButtonElement>
> = ({
  children,
  href,
  disabled = false,
  variant = 'contained',
  color = 'primary',
  type = 'button',
  ...rest
}) => {
  const classes = useButtonStyles();
  return (
    <MaterialButton
      classes={{
        root: classes.root,
        label: classes.label,
        contained: classes.contained,
        outlined: classes.outlined,
        containedSizeSmall: classes.containedSizeSmall,
      }}
      variant={variant}
      color={color}
      disabled={disabled}
      href={href}
      type={type}
      {...rest}
    >
      {children}
    </MaterialButton>
  );
};

export default Button;
