import React from 'react';
import { Radio as MaterialRadio } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { useRadioStyles } from './styles';

export interface RadioProps {
  label: string;
  disabled?: boolean;
  value: string;
}

export const Radio: React.FC<RadioProps> = ({ label, disabled, value }) => {
  const classes = useRadioStyles();

  return (
    <FormControlLabel
      value={value}
      classes={{
        label: classes.label,
      }}
      control={
        <MaterialRadio
          color="primary"
          classes={{
            root: classes.root,
          }}
        />
      }
      disabled={disabled}
      label={label}
    />
  );
};
