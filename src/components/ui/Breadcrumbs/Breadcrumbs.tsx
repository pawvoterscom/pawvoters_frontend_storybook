import React from 'react';
import {
  Breadcrumbs as MaterialBreadcrumbs,
  Typography,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { ArrowSeparator } from '../../icons';
import s from './Breadcrumbs.module.scss';

export type BreadcrumbItem = {
  id: number | string;
  href: string;
  label: React.ReactNode;
};

export interface BreadcrumbsProps {
  items: BreadcrumbItem[];
}

const Breadcrumbs: React.FC<
  BreadcrumbsProps & React.HTMLAttributes<HTMLDivElement>
> = ({ items, ...rest }) => {
  if (!items.length) {
    return null;
  }

  if (items.length === 1) {
    return <Link to={items[0].href}>{items[0].label}</Link>;
  }

  return (
    <MaterialBreadcrumbs
      aria-label="breadcrumb"
      separator={<ArrowSeparator />}
      {...rest}
    >
      {items.slice(0, items.length - 1).map((item: BreadcrumbItem) => (
        <Link key={item.id} to={item.href} className={s.link}>
          {item.label}
        </Link>
      ))}
      {
        <Typography className={s.disabled}>
          {items[items.length - 1].label}
        </Typography>
      }
    </MaterialBreadcrumbs>
  );
};

export default Breadcrumbs;
