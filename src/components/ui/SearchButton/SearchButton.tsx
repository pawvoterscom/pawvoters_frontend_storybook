import React from 'react';
import { RoundedButton } from "../RoundedButton";
import { SearchIcon } from "../../icons";
import cn from 'classnames';
import { Link } from 'react-router-dom';

interface Props {
  className?: string;
}

const SearchButton: React.FC<Props> = ( { className } ) => (
  <Link to="/search">
    <RoundedButton className={ cn( className ) }>
      <SearchIcon/>
    </RoundedButton>
  </Link>
);

export default SearchButton;