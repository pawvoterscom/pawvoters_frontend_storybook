import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { useSpring, animated } from 'react-spring/web.cjs'; // web.cjs is required for IE 11 support
import colors from 'theme/colors';
import { Close } from 'components/icons';
import { getScrollBarWidth } from 'lib/utils/scroll';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      background: colors.bg1,
      boxShadow: '5px 5px 30px rgba(65, 37, 23, 0.05)',
      borderRadius: 20,
      padding: '40px 50px',
      position: 'relative',
      '& button:hover svg': {
        fill: 'var(--primary)',
      },
      '@media (max-width: 425px)': {
        padding: '40px 30px',
      },
    },
    close: {
      position: 'absolute',
      top: 30,
      right: 30,
      border: 'none',
      boxShadow: 'none',
      outline: 'none',
      background: 'none',
      color: colors.gray2,
      '& svg': {
        transition: 'fill .2s',
        width: 17,
        height: 17,
      },
    },
    fadeWrapper: {
      display: 'flex',
      position: 'relative',
      overflowY: 'auto',
      flexDirection: 'column',
      maxHeight: '100%',
      padding: '15px',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        display: 'none',
        width: '0px',
      },
    },
  })
);

interface FadeProps {
  children?: React.ReactElement;
  in: boolean;
  onEnter?: () => {};
  onExited?: () => {};
}

interface SpringModalProps {
  opened: boolean;
  handleClose: () => void;
}

const Fade = React.forwardRef<HTMLDivElement, FadeProps>(function Fade(
  props,
  ref
) {
  const classes = useStyles();
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0, transform: 'scale(0.95)' },
    to: { opacity: open ? 1 : 0, transform: open ? 'scale(1)' : 'scale(0.95)' },
    onStart: () => {
      if (open) {
        const header = document.querySelector('header');
        const padRight = getScrollBarWidth();
        if (header) {
          header.style.paddingRight = `${padRight}px`;
        }
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    },
  });

  return (
    <animated.div
      ref={ref}
      style={style}
      {...other}
      className={classes.fadeWrapper}
    >
      {children}
    </animated.div>
  );
});

const SpringModal: React.FC<SpringModalProps> = ({
  handleClose,
  opened,
  children,
}) => {
  const classes = useStyles();

  const handleCloseModal = () => {
    handleClose();
    const header = document.querySelector('header');
    if (header) {
      header.style.paddingRight = '0';
    }
  };

  return (
    <Modal
      className={classes.modal}
      open={opened}
      onClose={handleCloseModal}
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={opened}>
        <div className={classes.paper}>
          <button
            className={classes.close}
            type="button"
            onClick={handleCloseModal}
          >
            <Close />
          </button>
          {children}
        </div>
      </Fade>
    </Modal>
  );
};

export default SpringModal;
