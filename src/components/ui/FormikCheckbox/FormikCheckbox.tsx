import React from 'react';
import {
  Checkbox as MaterialCheckbox,
  FormControlLabel,
} from '@material-ui/core';
import { useCheckboxStyles } from '../Checkbox/styles';
import { CheckboxProps } from '../Checkbox';
import { useField } from 'formik';

interface FormikCheckboxProps {
  customChange?: (value: any) => void;
  name: string;
}

export const FormikCheckbox: React.FC<
  Partial<CheckboxProps> &
    FormikCheckboxProps &
    React.HTMLAttributes<HTMLInputElement>
> = ({ label, disabled, name, customChange }) => {
  const classes = useCheckboxStyles();
  const [field] = useField({ name, type: 'checkbox' });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (disabled) return;

    if (customChange) {
      customChange(!field.value);
      return;
    }

    field.onChange(e);
  };

  return (
    <FormControlLabel
      name={name}
      disabled={disabled}
      classes={{
        label: classes.label,
      }}
      control={
        <MaterialCheckbox
          color="primary"
          classes={{
            root: classes.root,
          }}
          {...field}
          checked={field.value}
          onChange={handleChange}
        />
      }
      label={label}
    />
  );
};
