import React, { useEffect } from 'react';
import RadioGroup from '@material-ui/core/RadioGroup';
import { useField } from 'formik';
import { useAppDispatch } from 'store/store';
import { getBreeds } from 'store/slices/myPets/thunks';
import { getContest } from 'store/slices/contest/thunks';

interface FormikRadioGroup {
  name: string;
  value: string;
  setFieldValue?: any;
  fieldValue?: string;
  dispatch?: boolean;
}

export const FormikRadioGroup: React.FC<
  FormikRadioGroup & React.HTMLAttributes<HTMLInputElement>
> = ({
  name,
  value,
  children,
  className,
  setFieldValue,
  fieldValue,
  dispatch,
}) => {
  const [field, meta, helpers] = useField({ value, name });
  const _dispatch = useAppDispatch();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    helpers.setValue(event.target.value);

    if (setFieldValue) setFieldValue(`${fieldValue}`, '');
  };

  useEffect(() => {
    if (dispatch) {
      _dispatch(getBreeds({ petType: field.value }));
      _dispatch(getContest({ type: field.value }));
    }
  }, [field.value]);

  return (
    <RadioGroup
      name={name}
      value={value}
      onChange={handleChange}
      children={children}
      className={className}
    />
  );
};
