import React from 'react';
import { Select } from '../Select';
import { useField } from 'formik';
import { SelectProps } from '@material-ui/core';

interface FormikSelect {
  label: string;
  name: string;
  items: any;
  value?: string;
}

export const FormikSelect: React.FC<
  SelectProps & FormikSelect & React.HTMLAttributes<HTMLInputElement>
> = ({ label, name, items, value, disabled }) => {
  const [field, meta, helpers] = useField(name);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    helpers.setValue(event.target.value);
  };

  return (
    <Select
      label={label}
      name={name}
      items={items}
      value={value}
      disabled={disabled}
      customOnChange={handleChange}
    />
  );
};
