import React from 'react';
import s from './ContestSwitcher.module.scss';
import cn from 'classnames';

export type SwitcherItem = {
  id: number;
  label: string;
  value: string;
};

export interface SwitcherInterface {
  items: SwitcherItem[];
  onChange: any;
  active: string;
}

const ContestSwitcher: React.FC<SwitcherInterface> = ({
  items,
  active,
  onChange,
}) => {
  const handleClick = (type: string) => () => {
    if (type === active) return;
    onChange(type);
  };

  return (
    <div className={s.switchItem}>
      <span
        className={cn(s.indicator, {
          [s.firstItem]: active === items[0].value,
          [s.secondItem]: active === items[1].value,
          [s.thirdItem]: active === items[2].value,
        })}
      />
      {items.map((item: SwitcherItem, id) => (
        <span
          key={id}
          onClick={handleClick(item.value)}
          className={cn(s.tabItem, {
            [s.activeTab]: active === item.value,
          })}
        >
          {item.label}
        </span>
      ))}
    </div>
  );
};

export default ContestSwitcher;
