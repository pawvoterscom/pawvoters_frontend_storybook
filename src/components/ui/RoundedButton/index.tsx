import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { useRoundedButtonStyles } from './styles';

export interface RoundedButtonProps {
  disabled?: boolean;
  children: React.ReactNode;
  className?: string;
  onClick?: ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined;
}

export const RoundedButton: React.FC<RoundedButtonProps> = ({
  children,
  disabled = false,
  className,
  onClick
}) => {
  const classes = useRoundedButtonStyles();
  return (
    <IconButton
      classes={{
        root: classes.root,
      }}
      className={className}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </IconButton>
  );
};
