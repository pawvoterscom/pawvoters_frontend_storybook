import React from 'react';
import { ArrowTop } from "../../icons";
import cn from 'classnames';
import { RoundedButton } from "../RoundedButton";

interface Props {
  className?: string;
}

const ScrollTopButton: React.FC<Props> = ( { className } ) => {
  const handleClick = () => {
    window.scrollTo( {
      top: 0,
      behavior: "smooth"
    } );
  }

  return (
    <RoundedButton onClick={ handleClick } className={ cn( className ) }>
      <ArrowTop/>
    </RoundedButton>
  );
}

export default ScrollTopButton;