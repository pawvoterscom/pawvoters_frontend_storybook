import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useInputStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      position: 'relative',
      '& > svg': {
        position: 'absolute',
        left: 12,
        top: 14,
        pointerEvents: 'none',
        zIndex: 1,
      },
    },
    root: {
      width: '100%',
      '& > label': {
        fontSize: 13,
        transform: 'translate(11px, 16px) scale(1)',
        color: '#A4AFBE!important',
        '&.MuiInputLabel-focused.MuiInputLabel-shrink': {
          transform: 'translate(11px, 16px) scale(1)',
        },
        '&.Mui-focused': {
          transform: 'translate(12px, 10px) scale(0.75)',
        },
      },
      '&.success > div': {
        borderColor: '#9ECA74',
      },
      '&.multiline > label': {
        transform: 'translate(11px, 16px) scale(1)',
        '&.MuiInputLabel-focused': {
          transform: 'translate(12px, 13px) scale(0.75)',
        },
        '&.MuiInputLabel-shrink': {
          transform: 'translate(12px, 13px) scale(0.75)',
        },
      },
      '&.multiline .MuiFormHelperText-root': {
        top: '99%',
      },
      '&.select': {
        position: 'relative',
        '& svg': {
          position: 'absolute',
          right: 10,
          top: 13,
          width: 18,
          height: 18,
          pointerEvents: 'none',
        },
        '& > label': {
          transform: 'translate(13px, 16px) scale(1) !important',
        },
      },
      '&.startAdornment': {
        '& > label': {
          transform: 'translate(38px, 16px) scale(1)',
          '&.MuiInputLabel-shrink': {
            transform: 'translate(38px, 10px) scale(0.75)',
          },
          '&.Mui-focused': {
            transform: 'translate(38px, 10px) scale(0.75)',
          },
        },
        '& input': {
          paddingLeft: 37,
        },
      },
    },
    selectRoot: {
      maxHeight: 'calc(40% - 96px)',
      marginTop: 8,
      boxShadow: '2px 10px 30px rgba(72, 103, 138, 0.12)',
      borderRadius: 5,
    },
    selectList: {
      padding: '8px 9px 11px',
      '& > li': {
        padding: '11px 12px 9px',
        fontSize: 13,
        lineHeight: '16px',
        fontWeight: 500,
        borderRadius: 5,
        marginBottom: 5,
        '&:hover': {
          background: 'rgba(235, 134, 75, 0.1)',
        },
        '&:last-of-type': {
          marginBottom: 0,
        },
      },
      '& > li.Mui-selected': {
        background: 'rgba(235, 134, 75, 0.2)',
        pointerEvents: 'none',
        '&:hover': {
          background: 'rgba(235, 134, 75, 0.2)',
        },
      },
    },
  })
);
