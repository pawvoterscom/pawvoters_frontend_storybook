import React from 'react';
import { ToastProps } from '.';
import styles from './index.module.scss';

export const ErrorToast: React.FC<ToastProps> = ({
  title = 'Error',
  description,
}) => {
  return (
    <div className={styles.toastWrapper}>
      <div className={styles.toastIcon}>
        <svg
          width="14"
          height="14"
          viewBox="0 0 14 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M7.00001 13.6668C3.31801 13.6668 0.333344 10.6822 0.333344 7.00016C0.333344 3.31816 3.31801 0.333496 7.00001 0.333496C10.682 0.333496 13.6667 3.31816 13.6667 7.00016C13.6667 10.6822 10.682 13.6668 7.00001 13.6668ZM6.33334 9.00016V10.3335H7.66668V9.00016H6.33334ZM6.33334 3.66683V7.66683H7.66668V3.66683H6.33334Z"
            fill="white"
          />
        </svg>
      </div>
      <div className={styles.toastInfo}>
        <span className={styles.toastTitle}>{title}</span>
        <span className={styles.toastInfo}>{description}</span>
      </div>
    </div>
  );
};
