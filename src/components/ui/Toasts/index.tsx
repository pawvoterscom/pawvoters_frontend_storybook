import { ErrorToast } from './ErrorToast';
import { SuccessToast } from './SuccessToast';

export interface ToastProps {
  title?: string;
  description: React.ReactNode;
}

export { ErrorToast, SuccessToast };
