import React, { useEffect } from 'react';
import s from './SearchInput.module.scss';
import { getPosts } from 'store/slices/search/thunks';
import { useAppDispatch, useSelector } from 'store/store';
import {
  changeQuery,
  clearState,
  changeActualQuery,
} from 'store/slices/search';
import Button from 'components/ui/Button';
import { searchSelector } from 'store/selectors';
import { useHistory } from 'react-router';
import { Input } from 'components/ui/Input';

const SearchInputCard = () => {
  const dispatch = useAppDispatch();
  const posts = useSelector(searchSelector);
  const history = useHistory();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(changeQuery(e.target.value));
  };

  const handleClearInput = () => {
    dispatch(clearState());
  };

  const handleClick = () => {
    dispatch(changeActualQuery(posts.value.query));

    if (posts.value.query) {
      dispatch(getPosts({ query: posts.value.query }));
      history.push(`/search/posts?query=${posts.value.query}`);
    }
  };

  const handleEnter = (e: any) => {
    if (e.key === 'Enter') {
      handleClick();
    }
  };

  useEffect(() => {
    const location = history.location.search;
    const query = location.slice(7);

    if (query) {
      dispatch(changeActualQuery(query));
      dispatch(changeQuery(query));
      dispatch(getPosts({ query }));
    }
  }, [dispatch, history]);

  return (
    <div className={s.inputCard}>
      <div className={s.searchWrapper}>
        <Input
          label="Search"
          name="search"
          className={s.searchInput}
          value={posts.value.query}
          endIcon={
            posts.value.query ? (
              <svg
                className={s.closeIcon}
                onClick={handleClearInput}
                width="10"
                height="10"
                viewBox="0 0 10 10"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M5 4.05732L8.3 0.757324L9.24267 1.69999L5.94267 4.99999L9.24267 8.29999L8.3 9.24266L5 5.94266L1.7 9.24266L0.757334 8.29999L4.05733 4.99999L0.757334 1.69999L1.7 0.757324L5 4.05732Z" />
              </svg>
            ) : null
          }
          onChange={handleChange}
          onKeyPress={handleEnter}
        />
      </div>
      <Button className={s.searchButton} onClick={handleClick}>
        Search
      </Button>
    </div>
  );
};

export default SearchInputCard;
