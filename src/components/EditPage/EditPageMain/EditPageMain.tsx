import React, { useState } from 'react';
import s from './EditPageMain.module.scss';
import EditPageForm from '../EditPageForm';
import AddMedia from '../../common/AddMedia';
import { myPetsBreed } from 'store/selectors/myPets';
import { useSelector } from 'store/store';
import { PetAddImagePlaceholder } from '../EditPlaceholder';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { petDataSelector } from 'store/selectors/index';

const EditPageMain = () => {
  const myPetData = useSelector(petDataSelector);
  const petBreedsData = useSelector(myPetsBreed);

  const [file, setFile] = useState<File | string>('');

  const handleChangeFile = (_file: File) => setFile(_file);

  return (
    <div className={s.root}>
      {myPetData && petBreedsData.status === LoadingStatusEnum.SUCCESS ? (
        <AddMedia
          src={`${myPetData.image}_600x600.jpg`}
          maxFileSize={5242880}
          handleChangeFile={handleChangeFile}
          defaultFormat
          editMedia
        />
      ) : (
        <PetAddImagePlaceholder />
      )}
      <div className={s.editPageForm}>
        <EditPageForm _file={file} />
      </div>
    </div>
  );
};

export default EditPageMain;
