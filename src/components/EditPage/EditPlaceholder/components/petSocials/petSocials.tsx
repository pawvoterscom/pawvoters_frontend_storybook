import React from 'react';
import s from './petSocials.module.scss';

const petSocials = () => (
  <div className={s.root}>
    <div className={s.instagram}>
      <span></span>
    </div>
    <div className={s.facebook}>
      <span></span>
    </div>
  </div>
);

export default petSocials;
