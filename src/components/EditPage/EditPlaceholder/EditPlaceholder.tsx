import React from 'react';
import {
  PetTypePlaceholder,
  PetNamePlaceholder,
  PetBioPlaceholder,
  PetBreedPlaceholder,
  PetSocialsPlaceholder,
} from '../EditPlaceholder';

const EditPlaceholder = () => {
  return (
    <>
      <PetNamePlaceholder />
      <PetTypePlaceholder />
      <PetBreedPlaceholder />
      <PetBioPlaceholder />
      <PetSocialsPlaceholder />
    </>
  );
};

export default EditPlaceholder;
