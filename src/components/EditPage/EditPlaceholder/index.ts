export { default as PetTypePlaceholder } from './components/petType';
export { default as PetNamePlaceholder } from './components/petName';
export { default as PetBioPlaceholder } from './components/petBio';
export { default as PetBreedPlaceholder } from './components/petBreed';
export { default as PetAddImagePlaceholder } from './components/petAddImage';
export { default as PetSocialsPlaceholder } from './components/petSocials';
