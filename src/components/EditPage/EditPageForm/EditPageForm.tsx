import React, { useState, useEffect } from 'react';
import s from './EditPageForm.module.scss';
import FormikSelect from '../../ui/FormikSelect';
import { Typography } from '@material-ui/core';
import FormikInput from '../../ui/FormikInput';
import { Radio } from '../../ui/Radio';
import FormikRadioGroup from '../../ui/FormikRadioGroup';
import Button from '../../ui/Button';
import { useHistory } from 'react-router';
import { createErrorToast, createSuccessToast } from 'lib/utils/toasts';
import { createFormData } from 'lib/utils/form-data';
import { shallowEqual } from 'lib/utils/common';
import { Form, Formik } from 'formik';
import { useSelector } from 'store/store';
import { useAppDispatch } from 'store/store';
import * as Yup from 'yup';
import MainApiProtected from 'api/MainApiProtected';
import { myPetsBreed, myPetsBreedsData } from 'store/selectors/myPets';
import { petDataSelector } from 'store/selectors/index';
import { getBreeds } from 'store/slices/myPets/thunks';
import { resetBreed } from 'store/slices/myPets';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { fetchPostData } from 'store/slices/pet/thunks';
import EditPlaceholder from '../EditPlaceholder/EditPlaceholder';

type InitialValuesType = {
  petName: string;
  petType: string;
  breed: string;
  bio: string;
  fbLink: string;
  instaLink: string;
};

type EditPageFormType = {
  _file: File | string;
};

const EditPageForm: React.FC<EditPageFormType> = ({ _file }) => {
  const petBreedsData = useSelector(myPetsBreed);
  const petBreeds = useSelector(myPetsBreedsData);
  const _petData = useSelector(petDataSelector);

  const dispatch = useAppDispatch();
  const history = useHistory();

  const [initialValues, setInitialValues] = useState<InitialValuesType>({
    petName: '',
    petType: '',
    breed: '',
    bio: '',
    fbLink: '',
    instaLink: '',
  });

  useEffect(() => {
    (async () => {
      const id = history.location.pathname.match(RegExp(/[0-9]+/));
      const postId = Number(id);

      const result = await dispatch(fetchPostData({ postId }));

      if (result.payload?.status !== 200) history.push('/pets');
    })();
  }, []);

  useEffect(() => {
    if (_petData) {
      setInitialValues({
        petName: _petData.petName || '',
        petType: _petData.petType || '',
        breed: _petData.breed.name || '',
        bio: _petData.bio || '',
        fbLink: _petData.fbLink || '',
        instaLink: _petData.instaLink || '',
      });

      dispatch(getBreeds({ petType: _petData.petType }));
    }
  }, [_petData]);

  useEffect(() => {
    return () => {
      dispatch(resetBreed());
    };
  }, [dispatch]);

  const validationSchema = Yup.object({
    petName: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(60, 'Maximum 60 symbols')
      .required('Required field'),
    bio: Yup.string()
      .min(10, 'Minimum 10 symbols')
      .max(1000, 'Maximum 1000 symbols'),
    breed: Yup.string().required('Select Required'),
    petType: Yup.string().required('Select Required'),
  });

  const handleSubmit = async (_params: InitialValuesType): Promise<void> => {
    let params: any = { ..._params };

    if (_file) params.image = _file;

    try {
      const result = await MainApiProtected.getInstance().editMyPet(
        createFormData(params),
        _petData?.id
      );

      if (result.status === 200) {
        createSuccessToast({
          message: 'Your pet has been successfully saved',
        });

        return history.push('/pets');
      }

      createErrorToast({ message: 'Something went wrong, please try again' });
    } catch (error) {
      for (let i in error.data.errors)
        createErrorToast({ message: `${error.data.errors[i]}` });
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, initialValues, isValid, isSubmitting, setFieldValue }) => (
        <Form className={s.root}>
          {_petData && petBreedsData.status === LoadingStatusEnum.SUCCESS ? (
            <>
              <Typography variant="h4" className={s.informationTitle}>
                Information about your pet
              </Typography>
              <FormikInput disabled label="Pet Name *" name="petName" />
              <div className={s.radioGroupWrapper}>
                <Typography variant="body1" classes={{ root: s.petTypeTitle }}>
                  Pet's type
                </Typography>
                <FormikRadioGroup
                  aria-label="petType"
                  name="petType"
                  value={values.petType}
                  className={s.radioGroup}
                  setFieldValue={setFieldValue}
                  fieldValue="breed"
                >
                  <Radio disabled value="dog" label="Dog" />
                  <Radio disabled value="cat" label="Cat" />
                </FormikRadioGroup>
              </div>
              <div className={s.bottomed}>
                <FormikSelect
                  disabled
                  label="Breed *"
                  value={values.breed}
                  name="breed"
                  items={petBreeds}
                />
              </div>
              <div className={s.bottomed}>
                <FormikInput label="Bio" name="bio" multiline rows="6" />
              </div>
              <div className={s.bottomed}>
                <FormikInput label="Instagram" name="instaLink" />
              </div>
              <div className={s.bottomed}>
                <FormikInput label="Facebook" name="fbLink" />
              </div>
            </>
          ) : (
            <EditPlaceholder />
          )}
          <Button
            className={s.submitBtn}
            type="submit"
            disabled={
              (!_file && shallowEqual(values, initialValues)) ||
              !isValid ||
              isSubmitting
            }
          >
            Save
          </Button>
        </Form>
      )}
    </Formik>
  );
};

export default EditPageForm;
