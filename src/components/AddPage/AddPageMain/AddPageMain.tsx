import React, { useEffect, useState } from 'react';
import s from './AddPageMain.module.scss';
import AddMedia from '../../common/AddMedia';
import AddPageForm from '../AddPageForm';

export type DataType = {
  petName?: string;
  file?: File;
};

const AddPageMain = () => {
  const [file, setFile] = useState<File>();
  const result = localStorage.getItem('pet');

  let _parseResults = null;

  if (result) _parseResults = JSON.parse(result);

  useEffect(() => {
    return () => localStorage.removeItem('pet');
  });

  const handleChangeFile = (_file: File) => setFile(_file);

  return (
    <div className={s.root}>
      <AddMedia
        maxFileSize={5242880}
        editMedia
        defaultFormat
        handleChangeFile={handleChangeFile}
        src={_parseResults?.image}
      />
      <div className={s.addPageForm}>
        <AddPageForm petName={_parseResults?.petName} file={file} />
      </div>
    </div>
  );
};

export default AddPageMain;
