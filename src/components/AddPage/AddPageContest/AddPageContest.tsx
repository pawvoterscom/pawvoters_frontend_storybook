import React from 'react';
import s from './AddPageContest.module.scss';
import { Leaderboard } from '../../icons';
import { CircularProgress, Typography } from '@material-ui/core';
import { Checkbox } from '../../ui/Checkbox';
import { useSelector } from 'store/store';
import { contestDataSelector, contestSelector } from 'store/selectors';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { formattedDates } from 'lib/utils/formatted-dates';

export interface AddPageContestType {
  value: boolean;
  changeValue: (value: boolean) => void;
}

const AddPageContest: React.FC<AddPageContestType> = ({
  changeValue,
  value,
}) => {
  const contestData = useSelector(contestSelector);
  const [contest] = useSelector(contestDataSelector);

  const handleChange = () => changeValue(!value);

  const { startedAt, finishedAt } = contest || {};

  const dates = contest
    ? formattedDates({ date: startedAt, _date: finishedAt }, 'MMMM d')
    : [];

  const [date, _date] = dates;

  return (
    <div className={s.root}>
      {contestData.status !== LoadingStatusEnum.LOADING && contest ? (
        <>
          <div className={s.contestPreview}>
            <span>
              <Leaderboard />
            </span>
          </div>
          <div className={s.contestInfo}>
            <Typography variant="body1">
              {contest.title} in United States
            </Typography>
            <Typography variant="caption">
              From {date} To {_date}
            </Typography>
            <Checkbox
              name="participate"
              label="Participate in the contest"
              onChange={handleChange}
              value={value}
            />
          </div>
        </>
      ) : (
        <div className={s.preload}>
          <CircularProgress size={60} />
        </div>
      )}
    </div>
  );
};

export default AddPageContest;
