import React, { useState, useEffect } from 'react';
import s from './AddPageForm.module.scss';
import FormikSelect from '../../ui/FormikSelect';
import { Typography } from '@material-ui/core';
import { Radio } from '../../ui/Radio';
import FormikRadioGroup from '../../ui/FormikRadioGroup';
import Button from '../../ui/Button';
import AddPageContest from '../AddPageContest';
import * as Yup from 'yup';
import { shallowEqual } from 'lib/utils/common';
import { Form, Formik } from 'formik';
import { useSelector } from 'store/store';
import { myPetsDataSelector, myPetsBreedsData } from 'store/selectors/myPets';
import FormikInput from '../../ui/FormikInput';
import { createErrorToast, createSuccessToast } from 'lib/utils/toasts';
import MainApiProtected from 'api/MainApiProtected';
import { useHistory } from 'react-router';
import { createFormData } from 'lib/utils/form-data';
import { PetProps } from 'components/cards/Pet';
import { DataType } from '../AddPageMain/AddPageMain';
import { Media } from 'components/common/AddMedia/AddMedia';

interface ResultType {
  data: PetProps;
  status: number;
}

type InitialValuesType = {
  petName: string;
  petType: string;
  breed: string;
  bio: string;
  fbLink: string;
  instaLink: string;
};

const AddPageForm: React.FC<DataType> = ({ petName, file }) => {
  const [_checkBoxValue, setCheckBoxValue] = useState<boolean>(true);

  const myPetData = useSelector(myPetsDataSelector);
  const petBreeds = useSelector(myPetsBreedsData);
  const history = useHistory();

  const [initialValues, setInitialValues] = useState<InitialValuesType>({
    petName: '',
    petType: 'dog',
    breed: '',
    bio: '',
    fbLink: '',
    instaLink: '',
  });

  useEffect(() => {
    setInitialValues({
      petName: petName || '',
      petType: 'dog',
      breed: '',
      bio: '',
      fbLink: '',
      instaLink: '',
    });
  }, [myPetData]);

  const handleContest = (value: boolean) => setCheckBoxValue(value);

  const validationSchema = Yup.object({
    petName: Yup.string()
      .min(2, 'Minimum 2 symbols')
      .max(60, 'Maximum 60 symbols')
      .required('Required field'),
    bio: Yup.string()
      .min(10, 'Minimum 10 symbols')
      .max(1000, 'Maximum 1000 symbols'),
    breed: Yup.string().required('Select Required'),
    petType: Yup.string().required('Select Required'),
  });

  const handleSubmit = async (_params: InitialValuesType): Promise<void> => {
    const params = {
      image: Media.getFile() || file,
      participate: Number(_checkBoxValue),
      ..._params,
    };

    try {
      const _data: ResultType = await MainApiProtected.getInstance().postMyPet(
        createFormData(params)
      );

      if (_data.status === 201) {
        createSuccessToast({
          message: 'Your pet has been published successfully',
        });

        return history.push('/pets');
      }

      createErrorToast({
        message: 'Something went wrong, please try again',
      });
    } catch (error) {
      for (let i in error.data.errors)
        createErrorToast({ message: `${error.data.errors[i]}` });
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, initialValues, isValid, isSubmitting, setFieldValue }) => (
        <Form className={s.root}>
          <Typography variant="h4" className={s.informationTitle}>
            Information about your pet
          </Typography>
          <FormikInput label="Pet Name *" name="petName" />
          <div className={s.radioGroupWrapper}>
            <Typography variant="body1" classes={{ root: s.petTypeTitle }}>
              Pet's type
            </Typography>
            <FormikRadioGroup
              aria-label="petType"
              name="petType"
              value={values.petType}
              className={s.radioGroup}
              setFieldValue={setFieldValue}
              fieldValue="breed"
              dispatch
            >
              <Radio value="dog" label="Dog" />
              <Radio value="cat" label="Cat" />
            </FormikRadioGroup>
          </div>
          <div className={s.bottomed}>
            <FormikSelect
              label="Breed *"
              value={values.breed}
              name="breed"
              items={petBreeds}
            />
          </div>
          <div className={s.bottomed}>
            <FormikInput label="Bio" name="bio" multiline rows="6" />
          </div>
          <div className={s.bottomed}>
            <FormikInput label="Instagram" name="instaLink" />
          </div>
          <div className={s.bottomed}>
            <FormikInput label="Facebook" name="fbLink" />
          </div>
          <div className={s.bottomed}>
            <AddPageContest
              changeValue={handleContest}
              value={_checkBoxValue}
            />
          </div>
          <Button
            className={s.submitBtn}
            type="submit"
            disabled={
              shallowEqual(values, initialValues) ||
              !isValid ||
              isSubmitting ||
              !Media.getFile()
            }
          >
            Publish
          </Button>
        </Form>
      )}
    </Formik>
  );
};

export default AddPageForm;
