import React, { useEffect, useRef, useState } from 'react';
import { LoadMore as LoadMoreIcon } from 'components/icons';
import s from './LoadMoreButton.module.scss';
import { useAppDispatch } from 'store/store';
import { useSelector } from 'react-redux';
import { leaderboard } from 'store/selectors';
import { loadMorePosts } from 'store/slices/leaderboard/thunks';
import { createErrorToast } from 'lib/utils/toasts';
import Button from 'components/ui/Button';
import { CircularProgress } from '@material-ui/core';

interface Props {
  id: number;
  offset: number;
  limit: number;
}

const LoadMoreButton = ({ id, limit, offset }: Props) => {
  const dispatch = useAppDispatch();
  const leaderboardState = useSelector(leaderboard);
  const unmounted = useRef(false);

  useEffect(() => {
    return () => {
      unmounted.current = true;
    };
  }, []);

  const [buttonActive, setButtonActive] = useState<boolean>(false);

  const _leaderboardData = leaderboardState.data.length;

  const getPage = (): number => {
    let _page: number = 0;

    for (const [key, value] of Object.entries(
      leaderboardState.pageToLoadInfo
    )) {
      if (Number(key) === id) _page = value;
    }

    return _page;
  };

  const handleClick = async (): Promise<void> => {
    try {
      if (!unmounted.current) setButtonActive(false);

      setButtonActive(true);

      const result = await dispatch(
        loadMorePosts({
          id,
          limit,
          offset,
          page: getPage(),
        })
      );

      if (result.payload?.status === 201) {
        return createErrorToast({
          message: 'Something went wrong, please try again later',
        });
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      {_leaderboardData && (
        <div className={s.root}>
          {buttonActive ? (
            <CircularProgress size={32} />
          ) : (
            <Button onClick={handleClick} disabled={buttonActive}>
              <LoadMoreIcon />
              Show more
            </Button>
          )}
        </div>
      )}
    </>
  );
};

export default LoadMoreButton;
