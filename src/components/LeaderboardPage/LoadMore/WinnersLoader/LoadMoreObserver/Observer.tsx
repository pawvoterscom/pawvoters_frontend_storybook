import React, { memo, useEffect } from 'react';
import s from './Observer.module.scss';
import { useSelector, useAppDispatch } from 'store/store';
import { useInView } from 'react-intersection-observer';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { leaderboard } from 'store/selectors';
import { loadMoreObserverWinners } from 'store/slices/leaderboard/thunks';

const LoadMoreObserverWinners = () => {
  const dispatch = useAppDispatch();
  const leaderboardState = useSelector(leaderboard);
  const { ref, inView } = useInView({
    threshold: 0,
  });

  useEffect(() => {
    if (leaderboardState.status === LoadingStatusEnum.SUCCESS && inView) {
      dispatch(
        loadMoreObserverWinners({
          id: 1,
          page: leaderboardState.observerPageToLoad,
        })
      );
    }
  }, [inView]);

  return (
    <>
      {leaderboardState.data.length !== 0 && (
        <div className={s.root}>
          {Boolean(leaderboardState.links.next) &&
            leaderboardState.observerPageToLoad !== 0 && (
              <div ref={ref} className={s.observerMarker}></div>
            )}
        </div>
      )}
    </>
  );
};

export default memo(LoadMoreObserverWinners);
