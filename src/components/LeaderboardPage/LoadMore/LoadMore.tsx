import React from 'react';
import { LoadMore as LoadMoreIcon } from 'components/icons';
import Button from 'components/ui/Button';
import s from './LoadMore.module.scss';
import { useAppDispatch } from 'store/store';
import { useSelector } from 'react-redux';
import { leaderboard } from 'store/selectors';
import { loadMore } from 'store/slices/leaderboard/thunks';

const LoadMore = () => {
  const dispatch = useAppDispatch();
  const leaderboardState = useSelector(leaderboard);

  const handleClick = () => {
    if (loadMore)
      dispatch(
        loadMore({
          id: 1,
          page: 2,
        })
      );
  };

  return (
    <>
      {leaderboardState.data.length ? (
        <div className={s.root}>
          {Boolean(leaderboardState.links.next) && (
            <div className={s.container}>
              <Button onClick={handleClick}>
                <LoadMoreIcon />
                Show more
              </Button>
            </div>
          )}
        </div>
      ) : null}
    </>
  );
};

export default LoadMore;
