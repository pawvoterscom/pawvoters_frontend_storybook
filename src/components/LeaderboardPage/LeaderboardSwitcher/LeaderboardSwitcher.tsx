import React, { useEffect, useState } from 'react';
import s from './LeaderboardSwitcher.module.scss';
import { useHistory, useLocation } from 'react-router-dom';
import { useAppDispatch } from 'store/store';
import Switcher from 'components/ui/ContestSwitcher';
import {
  getLeaderboardById,
  getLeaderboardWinners,
} from 'store/slices/leaderboard/thunks';
import { getContest } from 'store/slices/contest/thunks';
import { elements } from './elements';
import { useSelector } from 'react-redux';
import { leaderboard } from 'store/selectors';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { createErrorToast } from 'lib/utils/toasts';
import { LeaderBoardType } from 'types/enum/LeaderBoardType.enum';
import { resetContestStatus } from 'store/slices/contest';

interface FilterType {
  _type: string;
  _contestId: string | null | undefined;
}

const LeaderboardSwitcher = () => {
  const [active, setActive] = useState<string>('dogs');

  const dispatch = useAppDispatch();
  const history = useHistory();
  const location = useLocation();

  const leaderboardData = useSelector(leaderboard);

  useEffect(() => {
    const contestChecker: string[] = location.search.split('&');
    const [type, contestId]: string[] = contestChecker;

    const filter: FilterType = {
      _type: type.slice(6),
      _contestId: contestId ? contestId.match(/[0-9]+/)?.shift() : null,
    };

    const { _type, _contestId } = filter;

    const validType: boolean =
      _type === LeaderBoardType.CATS ||
      _type === LeaderBoardType.DOGS ||
      _type === LeaderBoardType.WINNERS;

    (async () => {
      try {
        setActive(validType ? _type : LeaderBoardType.DOGS);

        if (!validType) history.push('/leaderboard');

        if (_type === LeaderBoardType.WINNERS && !contestId) {
          const _result = await dispatch(getLeaderboardWinners({ page: 1 }));

          const status = _result.payload?.status;

          if (status !== 200) {
            createErrorToast({
              message: 'Something went wrong, please try again',
            });
          }

          return;
        }

        if (contestId && _type === LeaderBoardType.WINNERS) {
          if (
            !contestId.match(/contestId=[1-9]+/) ||
            contestId.match(/[1-9]+/)?.shift() !== _contestId
          )
            return history.push(`/leaderboard?type=${_type}`);

          return dispatch(
            getLeaderboardWinners({ page: 1, contestId: Number(_contestId) })
          );
        } else if (_type === LeaderBoardType.WINNERS)
          history.push(`/leaderboard?type=${_type}`);

        if (!contestId) {
          const _result = await dispatch(
            getContest({
              type: validType ? _type.slice(0, -1) : 'dog',
            })
          );

          const status = _result.payload?.status;

          if (status === 200) {
            const id = _result.payload?.data.data[0].id;

            dispatch(getLeaderboardById({ id }));
            dispatch(resetContestStatus());

            return;
          }

          createErrorToast({
            message: 'Something went wrong, please try again',
          });
        }
      } catch (error) {
        console.log(error);
      }
    })();
  }, [location.search]);

  const handleClick = (type: string) => {
    if (leaderboardData.status === LoadingStatusEnum.SUCCESS) {
      history.push(`/leaderboard?type=${type}`);
    }
  };

  return (
    <div className={s.switcher}>
      <Switcher active={active} onChange={handleClick} items={elements} />
    </div>
  );
};

export default LeaderboardSwitcher;
