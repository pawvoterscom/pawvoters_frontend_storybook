import React from 'react';
import s from './LeaderboardList.module.scss';
import cn from 'classnames';
import { CircularProgress } from '@material-ui/core';
import {
  leaderboard,
  leaderboardContest,
  leaderboardData,
} from 'store/selectors';
import { useSelector } from 'store/store';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { Link, useHistory } from 'react-router-dom';
import { generatePetSlug } from 'lib/utils/url-utils';
import Avatar from 'components/ui/Avatar';
import { ordinal_suffix } from 'lib/utils/ordinal-suffix';
import { formatDistance } from 'date-fns/esm';
import LoadMoreObserver from '../LoadMore/WinnersLoader/LoadMoreObserver';
import LoadMoreButton from '../LoadMore/WinnersLoader/LoadMoreButton';

const LeaderboardList = () => {
  const history = useHistory();

  const leaderBoard = useSelector(leaderboard);
  const leaderBoardData = useSelector(leaderboardData);
  const leaderBoardContest = useSelector(leaderboardContest);

  const contest = history.location.search.match(/contestId=[0-9]+/)?.shift();

  const LoadMore = ({ item }: any) => (
    <>
      {item &&
      Object.keys(item.posts).length < item.contest.countContestants ? (
        <LoadMoreButton
          id={item.contest.id}
          offset={leaderBoard.offsetInfo[item.contest.id]}
          limit={10}
        />
      ) : null}
    </>
  );

  const Preloader = () => {
    return (
      <div className={s.preload}>
        <CircularProgress size={55} />
      </div>
    );
  };

  const MobPreloader = () => {
    return (
      <div className={s.minPreload}>
        <CircularProgress size={44} />
      </div>
    );
  };

  const OtherLeaderboards = () => {
    return (
      <>
        {contest ? (
          <div className={s.otherLeaderboards}>
            <span>Other leaderboards</span>
          </div>
        ) : null}
      </>
    );
  };

  return (
    <>
      {leaderBoard.status !== LoadingStatusEnum.SUCCESS &&
      !leaderBoard.firstLoad ? (
        <Preloader />
      ) : (
        <>
          {leaderBoardContest.id ? (
            <div className={cn(s.items, 'big')}>
              {leaderBoardData.map((item: any, id) => {
                return (
                  <Link
                    className={s.item}
                    to={`/post/${generatePetSlug(item.petName, item.id)}`}
                    key={id}
                  >
                    <div className={s.itemPlace}>{ordinal_suffix(id + 1)}</div>
                    <div className={s.itemImage}>
                      <Avatar
                        className={s.image}
                        src={`${item.image}_120x120.jpg`}
                      />
                    </div>
                    <div className={s.content}>
                      <div className={s.left}>
                        <span className={s.leftHeader}>{item.petName}</span>
                        <span className={s.leftVotes}>
                          {item.count_votes} votes
                        </span>
                        {item.prize ? (
                          <span className={s.leftPrize}>
                            1st place prize: {item.prize}
                          </span>
                        ) : null}
                      </div>
                      <div className={s.mobileImage}>
                        {item.image ? (
                          <Avatar
                            className={s.image}
                            src={`${item.image}_120x120.jpg`}
                          />
                        ) : (
                          <span className={s.imagePlaceholder}></span>
                        )}
                      </div>
                      <div className={s.right}>
                        <span className={s.rightContest}>
                          {leaderBoardContest.title}
                        </span>
                        <div className={s.rightMobileBlock}>
                          <span className={s.rightTime}>
                            {formatDistance(
                              new Date(leaderBoardContest.finishedAt),
                              new Date()
                            )}
                            <span> left</span>
                          </span>
                          <span className={s.rightContestants}>
                            4214 Contestants
                          </span>
                        </div>
                        {item.prize ? (
                          <span className={s.rightPrize}>
                            Prize: {item.prize}
                          </span>
                        ) : null}
                      </div>
                    </div>
                  </Link>
                );
              })}
              {leaderBoard.firstLoad &&
                leaderBoard.status === LoadingStatusEnum.LOADING && (
                  <MobPreloader />
                )}
              {leaderBoard.links.next &&
              leaderBoard.status === LoadingStatusEnum.SUCCESS ? (
                <LoadMore />
              ) : null}
            </div>
          ) : (
            <>
              <div className={s.leaderboardWinners}>
                {leaderBoardData.map((item, id) => {
                  return (
                    <div key={id}>
                      {id === 1 ? <OtherLeaderboards /> : null}
                      <div className={cn(s.items, 'big')}>
                        {item.posts.map((post, id) => {
                          return (
                            <Link
                              className={s.item}
                              to={`/post/${generatePetSlug(
                                post.petName,
                                post.id
                              )}`}
                              key={id}
                            >
                              <div className={s.itemPlace}>
                                {ordinal_suffix(id + 1)}
                              </div>
                              <div className={s.itemImage}>
                                <Avatar
                                  className={s.image}
                                  src={`${post.image}_120x120.jpg`}
                                />
                              </div>
                              <div className={s.content}>
                                <div className={s.left}>
                                  <span className={s.leftHeader}>
                                    {post.petName}
                                  </span>
                                  <span className={s.leftVotes}>
                                    {post.countVotes} votes
                                  </span>
                                  {post.prize ? (
                                    <span className={s.leftPrize}>
                                      1st place prize: {post.prize}
                                    </span>
                                  ) : null}
                                </div>
                                <div className={s.mobileImage}>
                                  <Avatar
                                    className={s.image}
                                    src={`${post.image}_120x120.jpg`}
                                  />
                                </div>
                                <div className={s.right}>
                                  <span className={s.rightContest}>
                                    {item.contest.title}
                                  </span>
                                  <div className={s.rightMobileBlock}>
                                    <span className={s.rightTime}>
                                      {formatDistance(
                                        new Date(item.contest.finishedAt),
                                        new Date()
                                      )}
                                      <span> left</span>
                                    </span>
                                    <span className={s.rightContestants}>
                                      4214 Contestants
                                    </span>
                                  </div>
                                  {post.prize ? (
                                    <span className={s.rightPrize}>
                                      Prize: {post.prize}
                                    </span>
                                  ) : null}
                                </div>
                              </div>
                            </Link>
                          );
                        })}
                        <LoadMore item={item} />
                      </div>
                    </div>
                  );
                })}
                <LoadMoreObserver />
              </div>
              {leaderBoard.firstLoad &&
                leaderBoard.status === LoadingStatusEnum.LOADING && (
                  <Preloader />
                )}
            </>
          )}
        </>
      )}
    </>
  );
};

export default LeaderboardList;
