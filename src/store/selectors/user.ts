import { RootState } from '../store';

export const getUserDataSelector = ({ user }: RootState) => user.data;
export const getUserIsAuthSelector = ({ user }: RootState) => user.isAuth;
export const getUserTokenSelector = ({ user }: RootState) => user.token;
export const getUserSelector = ({ user }: RootState) => user;

export const notificationDataSelector = ({ user }: RootState) =>
  user.notifications.data;
export const notificationSelector = ({ user }: RootState) => user.notifications;
