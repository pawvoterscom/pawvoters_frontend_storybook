import { RootState } from '../store';

export const myPetsDataSelector = ({ myPets }: RootState) =>
  myPets.myPetData.data;
export const myPetsBreedsData = ({ myPets }: RootState) =>
  myPets.petBreeds.data;
export const myPetsBreed = ({ myPets }: RootState) => myPets.petBreeds;
export const myPetsSelector = ({ myPets }: RootState) => myPets;
