import { createSlice } from '@reduxjs/toolkit';
import { initialState } from './initialState';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { getContest, getContestWinners, LoadMoreWinners } from './thunks';

const contestSlice = createSlice({
  name: 'contest',
  initialState,
  reducers: {
    resetContest: (state) => {
      state.data = initialState.data;
      state.status = LoadingStatusEnum.IDLE;
      state.meta = initialState.meta;
      state.links = initialState.links;
    },
    resetContestStatus: (state) => {
      state.status = LoadingStatusEnum.IDLE;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getContest.pending, (state) => {
      state.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(getContest.fulfilled, (state, { payload }) => {
      state.data = payload.data.data;
      state.meta = payload.data.meta;
      state.links = payload.data.links;

      state.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(getContestWinners.pending, (state) => {
      state.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(getContestWinners.fulfilled, (state, { payload }) => {
      state.data = payload.data.data;
      state.meta = payload.data.meta;
      state.links = payload.data.links;

      state.status = LoadingStatusEnum.SUCCESS;
      state.pageToLoad = state.pageToLoad + 1;
      state.firstLoad = false;
    });

    builder.addCase(LoadMoreWinners.pending, (state) => {
      state.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(LoadMoreWinners.fulfilled, (state, { payload }) => {
      state.data = [...state.data, ...payload.data.data];
      state.pageToLoad = state.pageToLoad + 1;
      state.meta = payload.data.meta;
      state.links = payload.data.links;

      state.status = LoadingStatusEnum.SUCCESS;
    });
  },
});

const { actions, reducer } = contestSlice;
export const { resetContest, resetContestStatus } = actions;
export default reducer;
