import { PaginatedMeta } from './../../../types/interfaces/common/PaginatedMeta.interface';
import { PaginatedLinks } from './../../../types/interfaces/common/PaginatedLinks.interface';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { ContestPost } from 'types/interfaces/ContestPost.interface';

type InitialState = {
  data: ContestPost[];
  error: null;
  meta: PaginatedMeta;
  links: PaginatedLinks;
  status: LoadingStatusEnum;
  pageToLoad: number;
  firstLoad: boolean;
};

export const initialState: InitialState = {
  data: [],
  error: null,
  links: {
    first: null,
    last: null,
    next: null,
    prev: null,
  },
  meta: {
    per_page: 0,
    total: 0,
  },
  status: LoadingStatusEnum.IDLE,
  pageToLoad: 1,
  firstLoad: true,
};
