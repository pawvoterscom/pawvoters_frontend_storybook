import { createAsyncThunk } from '@reduxjs/toolkit';
import MainApi from 'api/MainApi';
import { ContestPostType } from 'api/types/ContestPost.type';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';

export const getContest = createAsyncThunk<
  ContestPostType,
  { type?: string },
  {
    rejectValue: ApiErrorInterface;
  }
>('contest/getContest', async ({ type }, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getContest(type);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getContestWinners = createAsyncThunk<
  ContestPostType,
  {},
  {
    rejectValue: ApiErrorInterface;
  }
>('contest/getContestWinners', async (_, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getContestWinners();
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const LoadMoreWinners = createAsyncThunk<
  ContestPostType,
  { page?: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('contest/LoadMoreWinners', async ({ page }, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getContestWinners(page);
  } catch (error) {
    return rejectWithValue(error);
  }
});
