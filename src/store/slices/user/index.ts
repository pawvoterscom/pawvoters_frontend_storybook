import { createSlice } from '@reduxjs/toolkit';
import { initialState } from './initialState';
import { getNotifications, loadMore, getUnreadNotifications } from './thunks';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.data = action.payload;
      state.isAuth = true;
      state.token = action.payload.apiToken;
    },
    logoutUser: (state) => {
      state.token = null;
      state.isAuth = false;
      state.data = null;
    },
    resetNotifications: (state) => {
      state.notifications.data = initialState.notifications.data;
      state.notifications.links = initialState.notifications.links;
      state.notifications.meta = initialState.notifications.meta;
      state.notifications.error = initialState.notifications.error;
      state.notifications.firstLoad = initialState.notifications.firstLoad;
      state.notifications.pageToLoad = initialState.notifications.pageToLoad;
      state.notifications.opened = initialState.notifications.opened;

      state.notifications.status = LoadingStatusEnum.IDLE;
    },
    setModalOpened: (state, action) => {
      state.notifications.opened = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getNotifications.pending, (state) => {
      state.notifications.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(getNotifications.fulfilled, (state, { payload }) => {
      state.notifications.data = payload.data.data;
      state.notifications.meta = payload.data.meta;
      state.notifications.links = payload.data.links;

      state.notifications.pageToLoad = 2;
      state.notifications.firstLoad = true;
      state.notifications.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(getUnreadNotifications.fulfilled, (state, { payload }) => {
      state.notifications.unread = payload.data.count;
    });

    builder.addCase(loadMore.pending, (state) => {
      state.notifications.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(loadMore.fulfilled, (state, { payload }) => {
      state.notifications.data = [
        ...state.notifications.data,
        ...payload.data.data,
      ];
      state.notifications.links = payload.data.links;
      state.notifications.meta = payload.data.meta;

      state.notifications.pageToLoad = state.notifications.pageToLoad + 1;
      state.notifications.status = LoadingStatusEnum.SUCCESS;
    });
  },
});

const { actions, reducer } = userSlice;
export const {
  setModalOpened,
  setUser,
  logoutUser,
  resetNotifications,
} = actions;
export default reducer;
