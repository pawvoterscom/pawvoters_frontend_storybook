import { createSlice } from '@reduxjs/toolkit';
import { initialState } from './initialState';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { getPosts, loadMore, getPostsAutoComplete } from './thunks';

const searchSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    changeActualQuery: (state, { payload }) => {
      state.value.actualQuery = payload;
    },
    changeHeaderQuery: (state, { payload }) => {
      state.value.headerQuery = payload;
    },
    changeQuery: (state, { payload }) => {
      state.value.query = payload;
    },
    clearState: (state) => {
      state.data = [];
      state.value.query = '';

      state.status = LoadingStatusEnum.IDLE;
    },
    clearHeaderQuery: (state) => {
      state.value.headerQuery = '';
    },
    setAutoCompleteModal: (state, { payload }) => {
      state.autoCompleteModal = payload;
    },
    resetAutoCompleteData: (state) => {
      state.autoCompleteModal = false;
      state.autoCompleteData = [];
      state.value.headerQuery = '';
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getPosts.pending, (state) => {
      state.data = [];
      state.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(getPosts.rejected, (state, { payload }) => {
      state.data = [];
      state.status = LoadingStatusEnum.ERROR;
      state.error = payload?.data;
    });

    builder.addCase(getPostsAutoComplete.fulfilled, (state, { payload }) => {
      state.autoCompleteData = payload.data.data;
    });

    builder.addCase(getPosts.fulfilled, (state, { payload }) => {
      state.data = payload.data.data;
      state.links = payload.data.links;
      state.meta = payload.data.meta;

      state.pageToLoad = 2;
      state.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(loadMore.pending, (state) => {
      state.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(loadMore.rejected, (state, { payload }) => {
      state.error = payload?.data;
      state.status = LoadingStatusEnum.ERROR;
    });

    builder.addCase(loadMore.fulfilled, (state, { payload }) => {
      state.data = [...state.data, ...payload.data.data];
      state.links = payload.data.links;
      state.meta = payload.data.meta;

      state.pageToLoad = state.pageToLoad + 1;
      state.status = LoadingStatusEnum.SUCCESS;
    });
  },
});

const { actions, reducer } = searchSlice;
export const {
  changeActualQuery,
  changeQuery,
  clearState,
  changeHeaderQuery,
  clearHeaderQuery,
  setAutoCompleteModal,
  resetAutoCompleteData,
} = actions;
export default reducer;
