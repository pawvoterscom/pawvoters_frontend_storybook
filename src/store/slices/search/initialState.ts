import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { PaginatedLinks } from 'types/interfaces/common/PaginatedLinks.interface';
import { PaginatedMeta } from 'types/interfaces/common/PaginatedMeta.interface';
import { SearchPost } from 'types/interfaces/SearchPost.interface';
import { SearchValue } from 'types/interfaces/SearchValue.interface';

export type SearchInitialStateType = {
  data: SearchPost[];
  autoCompleteData: SearchPost[];
  status: LoadingStatusEnum;
  meta: PaginatedMeta;
  links: PaginatedLinks;
  value: SearchValue;
  error: null;
  autoCompleteModal: boolean;
  pageToLoad: number;
};

export const initialState = {
  data: [],
  autoCompleteData: [],
  error: null,
  links: {
    first: null,
    last: null,
    prev: null,
    next: null,
  },
  meta: {
    per_page: 16,
    total: 0,
  },
  pageToLoad: 2,
  autoCompleteModal: false,
  value: {
    query: '',
    actualQuery: '',
    headerQuery: '',
  },
  status: LoadingStatusEnum.IDLE,
} as SearchInitialStateType;
