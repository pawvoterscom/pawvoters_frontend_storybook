import { PaginatedMeta } from './../../../types/interfaces/common/PaginatedMeta.interface';
import { PaginatedLinks } from './../../../types/interfaces/common/PaginatedLinks.interface';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { LeaderboardPost } from 'types/interfaces/LeaderboardPost.interface';
import { Contest } from 'types/interfaces/common/Contest.interface';

export type OffsetInfo = {
  [key: string]: number;
};

export type PageToLoadInfo = {
  [key: string]: number;
};

type InitialState = {
  data: LeaderboardPost[];
  contest: Contest;
  error: null;
  meta: PaginatedMeta;
  links: PaginatedLinks;
  status: LoadingStatusEnum;
  firstLoad: boolean;
  observerPageToLoad: number;
  offsetInfo: OffsetInfo;
  pageToLoadInfo: PageToLoadInfo;
};

export const initialState: InitialState = {
  data: [],
  contest: {
    id: 0,
    title: '',
    startedAt: '',
    finishedAt: '',
  },
  error: null,
  links: {
    first: null,
    last: null,
    next: null,
    prev: null,
  },
  observerPageToLoad: 2,
  firstLoad: false,
  meta: {
    per_page: 0,
    total: 0,
  },
  status: LoadingStatusEnum.IDLE,
  offsetInfo: {},
  pageToLoadInfo: {},
};
