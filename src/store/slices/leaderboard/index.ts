import { createSlice } from '@reduxjs/toolkit';
import type { OffsetInfo, PageToLoadInfo } from './initialState';
import { initialState } from './initialState';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import {
  getLeaderboardWinners,
  getLeaderboardById,
  loadMore,
  loadMorePosts,
} from './thunks';

const leaderboardSlice = createSlice({
  name: 'leaderboard',
  initialState,
  reducers: {
    resetLeaderboard: (state) => {
      state.data = initialState.data;
      state.contest = initialState.contest;
      state.links = initialState.links;
      state.meta = initialState.meta;
      state.offsetInfo = initialState.offsetInfo;
      state.pageToLoadInfo = initialState.pageToLoadInfo;
      state.observerPageToLoad = initialState.observerPageToLoad;
      state.status = LoadingStatusEnum.IDLE;
      state.firstLoad = initialState.firstLoad;
      state.contest = initialState.contest;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getLeaderboardWinners.pending, (state) => {
      state.status = LoadingStatusEnum.LOADING;
      state.contest = initialState.contest;
      state.data = initialState.data;
      state.links = initialState.links;
      state.meta = initialState.meta;
      state.firstLoad = false;
    });

    builder.addCase(
      getLeaderboardWinners.fulfilled,
      (state, { payload: { data } }) => {
        const offsetInfo = {} as OffsetInfo;
        const pageToLoad = {} as PageToLoadInfo;

        data.data.forEach(({ contest, posts }: any) => {
          offsetInfo[contest.id] = posts.length;
          pageToLoad[contest.id] = 2;
        });

        state.offsetInfo = offsetInfo;
        state.pageToLoadInfo = pageToLoad;

        state.data = data.data;
        state.links = data.links;
        state.meta = data.meta;

        state.firstLoad = true;
        state.status = LoadingStatusEnum.SUCCESS;
      }
    );

    builder.addCase(getLeaderboardById.pending, (state) => {
      state.status = LoadingStatusEnum.LOADING;
      state.firstLoad = false;
    });

    builder.addCase(getLeaderboardById.fulfilled, (state, { payload }) => {
      state.data = payload.data.data;
      state.contest = payload.data.contest;
      state.links = payload.data.links;
      state.meta = payload.data.meta;

      state.firstLoad = true;
      state.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(loadMore.pending, (state) => {
      state.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(loadMore.fulfilled, (state, { payload: { data } }) => {
      state.data = [...state.data, ...data.data];
      state.links = data.links;
      state.meta = data.meta;

      const offsetInfo = {} as OffsetInfo;
      const pageToLoad = {} as PageToLoadInfo;

      data.data.forEach(({ contest, posts }: any) => {
        offsetInfo[contest.id] = posts.length;
        pageToLoad[contest.id] = 2;
      });

      state.offsetInfo = { ...state.offsetInfo, ...offsetInfo };
      state.pageToLoadInfo = { ...state.pageToLoadInfo, ...pageToLoad };

      state.observerPageToLoad = state.observerPageToLoad + 1;
      state.firstLoad = true;
      state.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(
      loadMorePosts.fulfilled,
      (state, { payload: { id, offset, page, data } }) => {
        state.pageToLoadInfo[id] = page + 1;
        state.offsetInfo[id] += offset;

        state.data.forEach((el) => {
          if (el.contest.id === id) {
            el.posts = [...el.posts, ...data.data];
          }
        });
      }
    );
  },
});

const { actions, reducer } = leaderboardSlice;
export const { resetLeaderboard } = actions;
export default reducer;
