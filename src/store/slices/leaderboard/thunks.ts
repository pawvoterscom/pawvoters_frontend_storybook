import { createAsyncThunk } from '@reduxjs/toolkit';
import MainApi from 'api/MainApi';
import { LeaderboardType } from 'api/types/LeaderboardPosts.type';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';

export const getLeaderboardWinners = createAsyncThunk<
  LeaderboardType,
  { page: number; contestId?: number },
  {
    rejectValue: ApiErrorInterface;
  }
>(
  'leaderboard/getLeaderboardWinners',
  async ({ contestId, page }, { rejectWithValue }) => {
    try {
      return await MainApi.getInstance().getLeaderboardWinners(page, contestId);
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getLeaderboardById = createAsyncThunk<
  LeaderboardType,
  { id: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('leaderboard/getLeaderboardById', async ({ id }, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getLeaderboardById(id, 1);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const loadMore = createAsyncThunk<
  LeaderboardType,
  { id: number; page: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('leaderboard/loadMore', async ({ id, page }, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getLeaderboardById(id, page);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const loadMoreObserverWinners = createAsyncThunk<
  LeaderboardType,
  { id: number; page: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('leaderboard/loadMore', async ({ id, page }, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getLeaderboardWinners(page, id);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const loadMorePosts = createAsyncThunk<
  LeaderboardType & {
    id: number;
    offset: number;
    page: number;
  },
  {
    id: number;
    limit: number;
    offset: number;
    page: number;
  },
  {
    rejectValue: ApiErrorInterface;
  }
>(
  'leaderboard/loadButton',
  async ({ id, limit, offset, page }, { rejectWithValue }) => {
    try {
      const res = await MainApi.getInstance().getMoreLeaderboardWinners(
        id,
        offset,
        limit,
        page
      );
      return {
        ...res,
        id,
        page,
        offset: limit,
      };
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getMoreLeaderboardWinners = createAsyncThunk<
  LeaderboardType,
  {
    id: number;
    offset: number;
    limit: number;
    page: number;
  },
  {
    rejectValue: ApiErrorInterface;
  }
>(
  'leaderboard/getMoreLeaderboard',
  async ({ id, offset, limit, page }, { rejectWithValue }) => {
    try {
      return await MainApi.getInstance().getMoreLeaderboardWinners(
        id,
        offset,
        limit,
        page
      );
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
