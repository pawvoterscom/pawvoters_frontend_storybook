import { createAsyncThunk } from '@reduxjs/toolkit';
import MainApi from 'api/MainApi';
import { HomePagePetsAPI, HomePostsType } from 'api/types/HomePosts.type';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';
import { HomeLastVotesType } from '../../../api/types/HomeLastVotes.type';
import { RootState } from '../../store';
import { HomeTestimonialsType } from '../../../api/types/HomeTestimonials.type';

export const fetchHomePagePets = createAsyncThunk<
  HomePostsType,
  HomePagePetsAPI,
  {
    rejectValue: ApiErrorInterface;
  }
>('home/loadPets', async ({ page, order }, { rejectWithValue, getState }) => {
  try {
    const state = getState() as RootState;

    return await MainApi.getInstance().getPosts({
      page,
      order: order || state.home.pets.order,
    });
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const fetchLastVotes = createAsyncThunk<
  HomeLastVotesType,
  void,
  {
    rejectValue: ApiErrorInterface;
  }
>('home/lastVotes', async (_, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getVotes();
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const fetchTestimonialItems = createAsyncThunk<
  HomeTestimonialsType,
  void,
  { rejectValue: ApiErrorInterface }
>('home/testimonials', async (_, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getTestimonials(1, 12, 1);
  } catch (error) {
    rejectWithValue(error);
  }
});
