import { createSlice } from '@reduxjs/toolkit';
import { LoadingStatusEnum } from 'types/enum/LoadingStatus.enum';
import { initialState, PetModalInterface } from './initialState';
import {
  createPostComment,
  deletePostComment,
  fetchPostComments,
  fetchPostData,
  fetchPostVotes,
  fetchSimilarPosts,
  getReportReasons,
} from './thunks';

const petSlice = createSlice({
  name: 'pet',
  initialState,
  reducers: {
    handlePetModalState: (
      state,
      { payload }: { payload: PetModalInterface }
    ) => {
      state.modalState = payload;
    },
    resetPetState: (state) => {
      state.petData.data = null;
      state.petData.status = LoadingStatusEnum.LOADING;
      state.petData.error = null;

      state.petVotesData.data = [];
      state.petVotesData.error = null;
      state.petVotesData.status = LoadingStatusEnum.LOADING;

      state.petCommentsData = initialState.petCommentsData;
    },
    updateIsReportedCommentState: (
      state,
      {
        payload,
      }: { payload: { commentId: number; parentId?: number; result: boolean } }
    ) => {
      if (payload.parentId) {
        const parentCommentIndex = state.petCommentsData.data.findIndex(
          (item) =>
            parseInt(String(item.id)) === parseInt(String(payload.parentId))
        );

        const repliedItem = state.petCommentsData.data[
          parentCommentIndex
        ].replies.find((item) => item.id === payload.commentId);

        if (repliedItem) {
          repliedItem.isAlreadyReportedByMe = payload.result;
        }
      } else {
        const commentIndex = state.petCommentsData.data.findIndex(
          (item) =>
            parseInt(String(item.id)) === parseInt(String(payload.commentId))
        );
        state.petCommentsData.data[commentIndex].isAlreadyReportedByMe =
          payload.result;
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchPostData.fulfilled, (state, { payload }) => {
      state.petData.data = payload.data;
      state.petData.error = null;
      state.petData.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(fetchPostData.pending, (state) => {
      state.petData.data = null;
      state.petData.error = null;
      state.petData.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(fetchPostData.rejected, (state, { payload }) => {
      state.petData.error = payload?.data;
      state.petData.status = LoadingStatusEnum.ERROR;
    });

    builder.addCase(fetchPostVotes.fulfilled, (state, { payload }) => {
      state.petVotesData.data = payload.data.splice(0, 4);
      state.petVotesData.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(fetchPostVotes.rejected, (state, { payload }) => {
      state.petVotesData.data = [];
      state.petVotesData.status = LoadingStatusEnum.ERROR;
      state.petVotesData.error = payload;
    });

    builder.addCase(fetchPostVotes.pending, (state) => {
      state.petVotesData.status = LoadingStatusEnum.LOADING;
      state.petVotesData.error = null;
      state.petVotesData.data = [];
    });

    builder.addCase(fetchPostComments.pending, (state, { meta }) => {
      if (meta.arg.parentId) {
        return;
      }

      if (state.petCommentsData.data.length > 0) {
        state.petCommentsData.loadingMoreStatus = LoadingStatusEnum.LOADING;
      } else {
        state.petCommentsData.status = LoadingStatusEnum.LOADING;
        state.petCommentsData.error = null;
        state.petCommentsData.data = [];
      }
    });

    builder.addCase(fetchPostComments.fulfilled, (state, { payload, meta }) => {
      if (meta.arg.parentId) {
        const parentCommentIndex = state.petCommentsData.data.findIndex(
          (item) =>
            parseInt(String(item.id)) === parseInt(String(meta.arg.parentId))
        );
        state.petCommentsData.data[parentCommentIndex].replies = [
          ...state.petCommentsData.data[parentCommentIndex].replies,
          ...payload.data.data,
        ];
        state.petCommentsData.loadingMoreStatus = LoadingStatusEnum.SUCCESS;
      } else {
        state.petCommentsData.loadingMoreStatus = LoadingStatusEnum.SUCCESS;
        state.petCommentsData.status = LoadingStatusEnum.SUCCESS;
        state.petCommentsData.data = [
          ...state.petCommentsData.data,
          ...payload.data.data,
        ];
        state.petCommentsData.links = payload.data.links;
        state.petCommentsData.meta = payload.data.meta;
        state.petCommentsData.isMore = Boolean(payload.data.links.next);

        if (Boolean(payload.data.links.next)) {
          state.petCommentsData.pageToLoad =
            state.petCommentsData.pageToLoad + 1;
        }
      }
    });

    builder.addCase(fetchSimilarPosts.fulfilled, (state, { payload }) => {
      state.similarPetsData.data = payload.data.data;
      state.similarPetsData.status = LoadingStatusEnum.SUCCESS;
    });

    builder.addCase(fetchSimilarPosts.pending, (state) => {
      state.similarPetsData.status = LoadingStatusEnum.LOADING;
      state.similarPetsData.error = null;
      state.similarPetsData.data = [];
    });

    builder.addCase(fetchSimilarPosts.rejected, (state, { payload }) => {
      state.similarPetsData.status = LoadingStatusEnum.ERROR;
      state.similarPetsData.error = payload;
      state.similarPetsData.data = [];
    });

    builder.addCase(createPostComment.pending, (state) => {
      state.petCommentsData.isCommenting = LoadingStatusEnum.LOADING;
    });

    builder.addCase(createPostComment.rejected, (state) => {
      state.petCommentsData.isCommenting = LoadingStatusEnum.IDLE;
    });

    builder.addCase(createPostComment.fulfilled, (state, { payload }) => {
      if (!payload.data.parentId) {
        state.petCommentsData.data = [
          payload.data,
          ...state.petCommentsData.data,
        ];
      } else {
        const parentCommentIndex = state.petCommentsData.data.findIndex(
          (item) =>
            parseInt(String(item.id)) ===
            parseInt(String(payload.data.parentId))
        );
        state.petCommentsData.data[parentCommentIndex].replies = [
          payload.data,
          ...state.petCommentsData.data[parentCommentIndex].replies,
        ];
      }

      state.petCommentsData.isCommenting = LoadingStatusEnum.IDLE;
    });

    builder.addCase(deletePostComment.pending, (state) => {
      state.petCommentsData.isDeleting = LoadingStatusEnum.LOADING;
    });

    builder.addCase(deletePostComment.fulfilled, (state, { meta }) => {
      if (meta.arg.parentId) {
        const commentIndex = state.petCommentsData.data.findIndex(
          (item) => Number(item.id) === Number(meta.arg.parentId)
        );

        state.petCommentsData.data[
          commentIndex
        ].replies = state.petCommentsData.data[commentIndex].replies.filter(
          (item) => Number(item.id) !== Number(meta.arg.commentId)
        );
      } else {
        state.petCommentsData.data = state.petCommentsData.data.filter(
          (item) => item.id !== meta.arg.commentId
        );
      }
      state.petCommentsData.isDeleting = LoadingStatusEnum.IDLE;
    });

    builder.addCase(deletePostComment.rejected, (state) => {
      state.petCommentsData.isDeleting = LoadingStatusEnum.IDLE;
    });

    builder.addCase(getReportReasons.pending, (state) => {
      state.reportReasons.status = LoadingStatusEnum.LOADING;
    });

    builder.addCase(getReportReasons.fulfilled, (state, { payload }) => {
      state.reportReasons.data = payload.data.data;
      state.reportReasons.status = LoadingStatusEnum.SUCCESS;
    });
  },
});

const { actions, reducer } = petSlice;
export const {
  handlePetModalState,
  resetPetState,
  updateIsReportedCommentState,
} = actions;
export default reducer;
