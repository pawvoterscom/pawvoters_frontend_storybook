import { PetResponseType } from 'api/types/PetResponse.type';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { ApiErrorInterface } from 'types/interfaces/common/ApiError.interface';
import MainApi from 'api/MainApi';
import { PetVotesResponseType } from 'api/types/PetVotesResponse.type';
import { PetCommentsResponseType } from 'api/types/PetCommentsResponse.type';
import { RootState } from '../../store';
import { PetSimilarResponseType } from 'api/types/PetSimilarResponse.type';
import { PetCommentResponseType } from '../../../api/types/PetCommentResponse.type';
import MainApiProtected from '../../../api/MainApiProtected';
import { DeletePostCommentType } from '../../../api/types/DeletePostComment.type';
import { ReasonsResponseType } from '../../../api/types/ReasonsResponse.type';

export const fetchPostData = createAsyncThunk<
  PetResponseType,
  { postId: number | string },
  {
    rejectValue: ApiErrorInterface;
  }
>('pet/loadPetData', async ({ postId }, { rejectWithValue }) => {
  try {
    return await MainApi.getInstance().getPostById(postId);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const fetchPostVotes = createAsyncThunk<
  PetVotesResponseType,
  { postId: number },
  {
    rejectValue: ApiErrorInterface;
  }
>('pet/loadPetVotesData', async ({ postId }, { rejectWithValue }) => {
  try {
    return MainApi.getInstance().getPostVotesById(postId);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const fetchPostComments = createAsyncThunk<
  PetCommentsResponseType,
  { postId: number; page?: number; parentId?: number },
  {
    rejectValue: ApiErrorInterface;
  }
>(
  'pet/loadPetComments',
  async ({ postId, parentId, page }, { rejectWithValue, getState }) => {
    try {
      const state = getState() as RootState;
      return MainApi.getInstance().getPostCommentsById(
        postId,
        page || state.pet.petCommentsData.pageToLoad,
        parentId
      );
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const fetchSimilarPosts = createAsyncThunk<
  PetSimilarResponseType,
  { postId: number },
  { rejectValue: ApiErrorInterface }
>('pet/loadSimilarPets', async ({ postId }, { rejectWithValue }) => {
  try {
    return MainApi.getInstance().getPostSimilarById(postId);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const createPostComment = createAsyncThunk<
  PetCommentResponseType,
  { postId: number; text: string; parentId?: number },
  { rejectValue: ApiErrorInterface }
>(
  'pet/createComment',
  async ({ postId, parentId, text }, { rejectWithValue }) => {
    try {
      return MainApiProtected.getInstance().postCommentById(
        postId,
        text,
        parentId
      );
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const deletePostComment = createAsyncThunk<
  DeletePostCommentType,
  { postId: number; commentId: number; parentId?: number },
  { rejectValue: ApiErrorInterface }
>('pet/deleteComment', ({ postId, commentId }, { rejectWithValue }) => {
  try {
    return MainApiProtected.getInstance().deleteCommentById(postId, commentId);
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getReportReasons = createAsyncThunk<
  ReasonsResponseType,
  {},
  { rejectValue: ApiErrorInterface }
>('pet/getReportReasons', (_, { rejectWithValue }) => {
  try {
    return MainApiProtected.getInstance().getReportsReasons();
  } catch (error) {
    rejectWithValue(error);
  }
});
