import { configureStore } from '@reduxjs/toolkit';
import appReducer from './slices/app';
import homePageReducer from './slices/home';
import petPageReducer from './slices/pet';
import userReducer from './slices/user';
import myPetsReducer from './slices/myPets';
import searchReducer from './slices/search';
import leaderboardReducer from './slices/leaderboard';
import contestReducer from './slices/contest';
import { createSelectorHook, useDispatch } from 'react-redux';

const store = configureStore({
  reducer: {
    app: appReducer,
    home: homePageReducer,
    pet: petPageReducer,
    user: userReducer,
    myPets: myPetsReducer,
    search: searchReducer,
    leaderboard: leaderboardReducer,
    contest: contestReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export const useSelector = createSelectorHook<RootState>();
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export default store;
