export enum HomeCardsEnum {
  LAST_VOTES = 'lastVotes',
  ADD_PET = 'addPet',
  WINNER_CARD = 'winnerCard'
}