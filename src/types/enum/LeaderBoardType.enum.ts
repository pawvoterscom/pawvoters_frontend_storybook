export enum LeaderBoardType {
  WINNERS = 'winners',
  CATS = 'cats',
  DOGS = 'dogs',
}
