export enum UserAuthEnum {
  GOOGLE = 'google',
  FACEBOOK = 'facebook',
  CUSTOM = 'custom',
}
