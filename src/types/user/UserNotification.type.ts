export type UserNotification = {
  id: number;
  type: string;
  link: string;
  isRead: boolean;
  createdAt: string;
  data: {
    contestId: number;
    contestImage: string;
    contestType: string;
    lastContestEndedAt: string;
    lastContestStartedAt: string;

    petId: number;
    petName: string;
    userNickname: string;
    userImage: string;
  };
};
