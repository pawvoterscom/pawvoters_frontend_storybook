export interface LeaderboardPost {
  contest: {
    id: number;
    title: string;
    startedAt: string | number;
    finishedAt: string | number;
  };
  posts: {
    id: number;
    petName: string;
    image: string;
    countVotes: number;
    place: number;
    prize: string;
  }[];
  countVotes: number;
  id: number;
  image: string;
  petName: string;
  place: number;
  prize: string;
}
