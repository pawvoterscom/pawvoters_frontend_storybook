export interface SearchValue {
  query: string;
  actualQuery: string;
  headerQuery: string;
}
