export interface SearchPost {
  id: number | string;
  petName: string;
  petType: 'cat' | 'dog';
  image: string;
}
