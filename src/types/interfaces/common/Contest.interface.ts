export interface Contest {
  id: number;
  title: string;
  startedAt: string;
  finishedAt: string;
}
