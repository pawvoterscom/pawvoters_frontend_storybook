export interface PaginatedLinks {
  first: string | null;
  last: string | null;
  next: string | null;
  prev: string | null;
}
