import { PaginatedMeta } from './PaginatedMeta.interface';
import { PaginatedLinks } from './PaginatedLinks.interface';
import { Contest } from './Contest.interface';

export interface PaginatedResponseInterface {
  meta: PaginatedMeta;
  links: PaginatedLinks;
  contest: Contest;
}
