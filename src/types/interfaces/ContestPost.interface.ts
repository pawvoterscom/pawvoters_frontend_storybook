export interface ContestPost {
  id: string | number;
  title: string;
  image: string;
  backgroundColor: string;
  type: string;
  status: string;
  firstPlaceWinnerName: string;
  countVotes: string | number;
  countContestants: number;
  firstPlacePrize: string | number;
  startedAt: string;
  finishedAt: string;
  timeToFinish: string | number;
  countryId: null | number;
}
