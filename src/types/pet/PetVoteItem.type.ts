export type PetVoteItemType = {
  id: number;
  created: string;
  userId: number;
  userImage: string;
  userNickname: string;
  postId: number;
}