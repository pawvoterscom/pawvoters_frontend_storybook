import HttpClient from './HttpClient';
import { HomePagePetsAPI } from './types/HomePosts.type';
import { UserAuthEnum } from '../types/enum/UserAuth.enum';
import { LoginInitResponseType } from './types/LoginInitResponse.type';

class MainApi extends HttpClient {
  private static classInstance?: MainApi;

  private constructor() {
    super('https://api-dev.pawvoters.com/v1');
  }

  public static getInstance() {
    if (!this.classInstance) {
      this.classInstance = new MainApi();
    }

    return this.classInstance;
  }

  public getPosts = async ({ page, order = 'popular' }: HomePagePetsAPI) => {
    const query = this.generateQuery({ page, per_page: 16, order });

    try {
      return this.instance.get(`/posts?${query}`);
    } catch (error) {
      return error;
    }
  };

  public getVotes = async () => {
    const query = this.generateQuery({ limit: 6 });
    try {
      return this.instance.get(`/votes?${query}`);
    } catch (error) {
      return error;
    }
  };

  public getPostById = async (postId: number | string) => {
    try {
      return this.instance.get(`/posts/${postId}`);
    } catch (error) {
      return error;
    }
  };

  public getPostVotesById = async (postId: number) => {
    try {
      return this.instance.get(`/posts/${postId}/votes`);
    } catch (error) {
      return error;
    }
  };

  public getPostCommentsById = async (
    postId: number,
    page?: number,
    parentId?: number
  ) => {
    const query = this.generateQuery({
      page,
      per_page: 6,
      parentId,
      repliesLimit: 6,
    });
    try {
      return this.instance.get(`/posts/${postId}/comments?${query}`);
    } catch (error) {
      return error;
    }
  };

  public getPostSimilarById = async (postId: number) => {
    const query = this.generateQuery({ per_page: 6 });
    try {
      return this.instance.get(`/posts/${postId}/similar?${query}`);
    } catch (error) {
      return error;
    }
  };

  public postLogin = async (type: UserAuthEnum, code: string) => {
    try {
      return this.instance.post('/login', {
        type,
        code,
      });
    } catch (error) {
      return error;
    }
  };

  public postLoginInit = async (
    email: string,
    action?: 'auth'
  ): Promise<LoginInitResponseType> => {
    try {
      return await this.instance.post('/login/init', {
        email,
        action: action || 'auth',
      });
    } catch (error) {
      return error;
    }
  };

  public postLoginConfirm = async (hash: string) => {
    try {
      return await this.instance.post('/login/confirm', {
        hash,
      });
    } catch (error) {
      return error;
    }
  };

  public postUsers = async (body: any) => {
    try {
      return await this.instance.post('/users', body);
    } catch (error) {
      return error;
    }
  };

  public getTestimonials = async (
    page: number,
    per_page: number,
    random: 1 | 0
  ) => {
    const query = this.generateQuery({ page, per_page, random });

    try {
      return await this.instance.get(`/testimonials?${query}`);
    } catch (error) {
      return error;
    }
  };

  public getSearchQuery = async (
    query: string,
    page: number,
    per_page?: number
  ) => {
    const params = this.generateQuery({
      per_page: per_page || 16,
      query,
      page,
    });
    try {
      return this.instance.get(`/search/posts?${params}`);
    } catch (error) {
      return error;
    }
  };

  public getContest = async (type?: string) => {
    try {
      return this.instance.get(type ? `/contests?type=${type}` : '/contests');
    } catch (error) {
      return error;
    }
  };

  public getContestWinners = async (page?: number) => {
    const params = this.generateQuery({
      per_page: 10,
      page,
    });

    try {
      return this.instance.get(
        page ? `/contests/winners?${params}` : `/contests/winners`
      );
    } catch (error) {
      return error;
    }
  };

  public getLeaderboardById = async (id: number, page: number) => {
    try {
      const params = this.generateQuery({
        per_page: 10,
        page,
      });
      return this.instance.get(`/contests/${id}/leaderboard?${params}`);
    } catch (error) {
      return error;
    }
  };

  public getLeaderboardWinners = async (
    page: number,
    contestId?: number | undefined
  ) => {
    try {
      const params = this.generateQuery({
        per_page: 3,
        page,
        contestId,
      });
      return this.instance.get(`/contests/winners/leaderboard?${params}`);
    } catch (error) {
      return error;
    }
  };

  public getMoreLeaderboardWinners = async (
    id: number,
    offset: number,
    limit: number = 10,
    page: number
  ) => {
    try {
      return this.instance.get(
        `/contests/${id}/leaderboard?limit=${limit}&offset=${offset}&page=${page}`
      );
    } catch (error) {
      return error;
    }
  };
}

export default MainApi;
