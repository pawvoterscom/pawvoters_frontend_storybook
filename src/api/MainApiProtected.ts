import HttpClient from './HttpClient';
import { AxiosRequestConfig } from 'axios';
import User from '../lib/User';
import { UserType } from '../types/user/User.type';

class MainApiProtected extends HttpClient {
  private static classInstance?: MainApiProtected;

  private constructor() {
    super('https://api-dev.pawvoters.com/v1');

    this._initializeRequestInterceptor();
  }

  private _initializeRequestInterceptor = () => {
    this.instance.interceptors.request.use(
      this._handleRequest,
      this._handleError
    );
  };

  private _handleRequest = (config: AxiosRequestConfig) => {
    const user: UserType = User.getUser();
    config.headers['Authorization'] = `Bearer ${user.apiToken}`;

    return config;
  };

  public static getInstance() {
    if (!this.classInstance) {
      this.classInstance = new MainApiProtected();
    }

    return this.classInstance;
  }

  public postCommentById(postId: number, text: string, parentId?: number) {
    try {
      const query = this.generateQuery({ parentId });
      return this.instance.post(`/posts/${postId}/comments?${query}`, {
        text,
      });
    } catch (error) {
      return error;
    }
  }

  public deleteCommentById(postId: number, commentId: number) {
    try {
      return this.instance.delete(`/posts/${postId}/comments/${commentId}`);
    } catch (error) {
      return error;
    }
  }

  public getUsersMe = async () => {
    try {
      return this.instance.get(`/users/me`);
    } catch (error) {
      return error;
    }
  };

  public getReportsReasons() {
    try {
      return this.instance.get('/reports/comments/reasons');
    } catch (error) {
      return error;
    }
  }

  public reportCommentById(commentId: number, reason: string) {
    try {
      this.instance.post(`/reports/comments/${commentId}`, {
        reason,
      });
    } catch (error) {
      return error;
    }
  }

  public getMyPets(page: number) {
    const query = this.generateQuery({ page: page, per_page: 12 });
    try {
      return this.instance.get(`/users/me/posts?${query}`);
    } catch (error) {
      return error;
    }
  }

  public postUsersMe = async (body: any) => {
    try {
      return this.instance.post('/users/me', body);
    } catch (error) {
      return error;
    }
  };

  public deleteUsersMe = async () => {
    try {
      return this.instance.delete('/users/me');
    } catch (error) {
      return error;
    }
  };

  public getNotifications = async (page: number) => {
    const query = this.generateQuery({ per_page: 6, page });
    try {
      return await this.instance.get(`/users/me/notifications?${query}`);
    } catch (error) {
      return error;
    }
  };

  public getUnreadNotifications = async () => {
    try {
      return this.instance.get('/users/me/notifications/unread-count');
    } catch (error) {
      return error;
    }
  };

  public postNotifications = async (body: any) => {
    try {
      return this.instance.post('/users/me/notifications/read', body);
    } catch (error) {
      return error;
    }
  };

  public postMyPet = async (body: any) => {
    try {
      return this.instance.post('/users/me/posts', body, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
    } catch (error) {
      return error;
    }
  };

  public editMyPet = async (body: any, postId?: string | number) => {
    try {
      return this.instance.post(`/users/me/posts/${postId}`, body, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
    } catch (error) {
      return error;
    }
  };

  public deleteMyPet = async (postId?: number | string) => {
    try {
      return this.instance.delete(`/users/me/posts/${postId}`);
    } catch (error) {
      return error;
    }
  };

  public getBreeds = async (petType: string) => {
    try {
      return this.instance.get(`/posts/breeds?petType=${petType}`);
    } catch (error) {
      return error;
    }
  };

  public postContactUs = async (body: any) => {
    try {
      return this.instance.post('/support', body);
    } catch (error) {
      return error;
    }
  };
}

export default MainApiProtected;
