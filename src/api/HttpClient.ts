import axios, { AxiosInstance, AxiosResponse } from 'axios';
import queryString from 'query-string';

declare module 'axios' {
  interface AxiosResponse<T = any> extends Promise<T> {}
}

abstract class HttpClient {
  protected readonly instance: AxiosInstance;

  protected constructor(baseURL: string) {
    this.instance = axios.create({
      baseURL,
    });

    this._initializeResponseInterceptor();
  }

  private _initializeResponseInterceptor = () => {
    this.instance.interceptors.response.use(
      this._handleResponse,
      this._handleError
    );
  };

  private _handleResponse = ({ data, status }: AxiosResponse<any>) => {
    return { data, status } as any;
  };

  protected _handleError = ({ response }: { response: AxiosResponse }) => {
    return Promise.reject({ data: response.data, status: response.status });
  };

  protected generateQuery(queryParams: Record<string, any>) {
    return queryString.stringify(queryParams);
  }
}

export default HttpClient;
