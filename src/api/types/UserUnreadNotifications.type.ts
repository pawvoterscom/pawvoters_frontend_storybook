import { ApiSuccessInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { UserUnreadNotifications } from 'types/user/UserUnreadNotifications.type';

export type UserUnreadNotificationsType = ApiSuccessInterface<UserUnreadNotifications>;
