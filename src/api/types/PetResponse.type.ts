import { ApiSuccessInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { PetType } from 'types/pet/Pet.type';

export type PetResponseType = ApiSuccessInterface<PetType>;
