import { ApiSuccessPaginatedInterface } from '../../types/interfaces/common/ApiSuccess.interface';
import { TestimonialItemType } from '../../types/home/TestimonialItem.type';

export type HomeTestimonialsType = ApiSuccessPaginatedInterface<
  TestimonialItemType[]
>;
