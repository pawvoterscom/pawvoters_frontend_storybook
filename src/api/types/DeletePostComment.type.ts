import { ApiSuccessCommonInterface } from '../../types/interfaces/common/ApiSuccess.interface';

export type DeletePostCommentType = ApiSuccessCommonInterface<{
  success: boolean;
  message: string;
}>;
