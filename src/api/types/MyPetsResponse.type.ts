import { ApiSuccessPaginatedInterface } from '../../types/interfaces/common/ApiSuccess.interface';
import { HomePost } from '../../types/interfaces/HomePost.interface';

export type MyPetsResponseType = ApiSuccessPaginatedInterface<HomePost[]>;
