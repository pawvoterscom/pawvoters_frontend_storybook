import { ApiSuccessPaginatedInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { ContestPost } from 'types/interfaces/ContestPost.interface';

export type ContestPostType = ApiSuccessPaginatedInterface<ContestPost[]>;
