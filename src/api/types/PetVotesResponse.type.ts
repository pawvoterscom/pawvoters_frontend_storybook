import { ApiSuccessInterface } from 'types/interfaces/common/ApiSuccess.interface';
import { PetVoteItemType } from "types/pet/PetVoteItem.type";

export type PetVotesResponseType = ApiSuccessInterface<PetVoteItemType[]>;
