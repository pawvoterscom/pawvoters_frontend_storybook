import { UserType } from '../types/user/User.type';

export enum UserStatuses {
  NO_USER = 'NO_USER',
  IS_AUTH = 'IS_AUTH',
}

class User {
  public static getUser() {
    const lsUser = localStorage.getItem('user');

    if (!lsUser) {
      return UserStatuses.NO_USER;
    }

    return JSON.parse(lsUser);
  }

  public static setUser(user: UserType): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public static logout() {
    localStorage.removeItem('user');
    window.location.reload();
  }

  public static isUser() {
    return this.getUser() !== UserStatuses.NO_USER;
  }
}

export default User;
