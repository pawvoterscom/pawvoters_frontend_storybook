export const checkFileType = (type: string) => {
  const types = ['image/jpg', 'image/jpeg', 'image/png'];

  const checkType = (types: string) => type === types;

  return types.some(checkType);
};
