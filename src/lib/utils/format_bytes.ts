export const formatBytes = (bytes: number, d = 2) => {
  if (bytes === 0) return '0 Bytes';

  const k: number = 1024;

  const _d: number = d < 0 ? 0 : d;

  const sizes: string[] = [
    'Bytes',
    'KB',
    'MB',
    'GB',
    'TB',
    'PB',
    'EB',
    'ZB',
    'YB',
  ];

  const i: number = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(_d)) + ' ' + sizes[i];
};
