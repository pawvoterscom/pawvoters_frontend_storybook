import { HomePost } from 'types/interfaces/HomePost.interface';
import { getRandomInt, getUuid } from './common';
import { HomeCardsEnum } from '../../types/enum/HomeCards.enum';

const cardTypes: HomeCardsEnum[] = [
  HomeCardsEnum.ADD_PET,
  HomeCardsEnum.WINNER_CARD,
];

type PetsItem = { id: string | number } & Partial<HomePost>;

export const homePetsPrepare = (data: HomePost[]): HomePost[] => {
  const newData: PetsItem[] = [
    {
      id: getUuid(),
      type: HomeCardsEnum.LAST_VOTES,
    },
    ...data,
  ];

  const randomInt = getRandomInt(2);

  if (data.length > 8) {
    newData.splice(8, 0, {
      id: getUuid(),
      type: cardTypes[Math.round(randomInt)],
    });
  }

  return newData as HomePost[];
};
