import { formatWithOptions } from 'date-fns/fp';

type formattedDatesType = (
  dates: {
    date: string;
    _date: string;
  },
  format?: string
) => string[];

export const formattedDates: formattedDatesType = ({ date, _date }, format) => {
  const toLower = (arg: any) => String(arg).toLowerCase();
  const dateToString = formatWithOptions({}, `${format}`);

  if (!_date) {
    let dates = [new Date(date)];

    return dates.map(dateToString).map(toLower);
  }

  let dates = [new Date(date), new Date(_date)];

  return dates.map(dateToString).map(toLower);
};
