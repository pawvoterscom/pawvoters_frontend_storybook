export const createFormData = (values: any): FormData => {
  const form_data = new FormData();

  if (values.emailNotificationSettings) {
    for (let key in values.emailNotificationSettings) {
      form_data.append(
        `emailNotificationSettings[${key}]`,
        String(Number(values.emailNotificationSettings[key]))
      );
    }
  }

  for (let key in values) {
    if (key !== 'emailNotificationSettings') {
      form_data.append(key, values[key]);
    }
  }

  return form_data;
};
