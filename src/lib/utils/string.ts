export const uppercaseFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const transformName = (name: string | undefined) => {
  try {
    if (name) {
      const nameParts = name.split(' ');
      return nameParts[0];
    }

    return name;
  } catch (error) {
    console.log(error);
    return name;
  }
};
