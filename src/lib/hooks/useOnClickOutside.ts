import React, { useEffect } from 'react';

interface RefTarget {
  contains(target: EventTarget | null): any;
}

const useOnClickOutside = (
  ref: React.RefObject<RefTarget>,
  handler: () => void,
  opened: boolean,
  hidden?: boolean
) => {
  useEffect(() => {
    const listener = (event: MouseEvent | TouchEvent) => {
      if (!ref.current || ref.current.contains(event.target)) return;

      handler();
    };

    document.addEventListener('click', listener);

    if (opened && hidden) document.body.style.overflow = 'hidden';

    return () => {
      document.removeEventListener('click', listener);
      document.body.style.overflow = '';
    };
  }, [ref, handler]);
};

export default useOnClickOutside;
